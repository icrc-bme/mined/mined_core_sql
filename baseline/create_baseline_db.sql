CREATE SCHEMA demographic;
CREATE SCHEMA recording;
CREATE SCHEMA signal;
CREATE SCHEMA "space";
CREATE SCHEMA util;

-- demographic.engel_scale definition

-- Drop table

-- DROP TABLE demographic.engel_scale;

CREATE TABLE demographic.engel_scale (
	id serial NOT NULL,
	scale_name varchar(32) NOT NULL,
	CONSTRAINT engel_scale_pk PRIMARY KEY (id),
	CONSTRAINT engel_scale_un UNIQUE (scale_name)
);


-- demographic.handedness definition

-- Drop table

-- DROP TABLE demographic.handedness;

CREATE TABLE demographic.handedness (
	id smallserial NOT NULL,
	handedness_name varchar(512) NOT NULL,
	CONSTRAINT handedness_pk PRIMARY KEY (id)
);


-- demographic.histopathology definition

-- Drop table

-- DROP TABLE demographic.histopathology;

CREATE TABLE demographic.histopathology (
	id serial NOT NULL,
	histopathology_name varchar(1024) NOT NULL,
	CONSTRAINT histopathology_pk PRIMARY KEY (id),
	CONSTRAINT histopathology_un UNIQUE (histopathology_name)
);


-- demographic.ilae_scale definition

-- Drop table

-- DROP TABLE demographic.ilae_scale;

CREATE TABLE demographic.ilae_scale (
	id serial NOT NULL,
	scale_name varchar(32) NULL,
	CONSTRAINT ilae_scale_un UNIQUE (scale_name),
	CONSTRAINT ilea_scale_pk PRIMARY KEY (id)
);

-- demographic.intervention definition

-- Drop table

-- DROP TABLE demographic.intervention;

CREATE TABLE demographic.intervention (
	id serial NOT NULL,
	intervention_name varchar(1024) NOT NULL,
	CONSTRAINT intervention_pk PRIMARY KEY (id),
	CONSTRAINT intervention_un UNIQUE (intervention_name)
);


-- demographic.sex definition

-- Drop table

-- DROP TABLE demographic.sex;

CREATE TABLE demographic.sex (
	id smallserial NOT NULL,
	sex_name varchar(512) NULL,
	CONSTRAINT sex_pk PRIMARY KEY (id)
);


-- demographic.subject_type definition

-- Drop table

-- DROP TABLE demographic.subject_type;

CREATE TABLE demographic.subject_type (
	id serial NOT NULL,
	type_name varchar(1024) NULL,
	CONSTRAINT subject_type_pk PRIMARY KEY (id),
	CONSTRAINT subject_type_un UNIQUE (type_name)
);

-- demographic.time_zone definition

-- Drop table

-- DROP TABLE demographic.time_zone;

CREATE TABLE demographic.time_zone (
	id serial NOT NULL,
	zone_name varchar(512) NOT NULL,
	CONSTRAINT time_zone_pk PRIMARY KEY (id),
	CONSTRAINT time_zone_un UNIQUE (zone_name)
);

-- demographic.institution definition

-- Drop table

-- DROP TABLE demographic.institution;

CREATE TABLE demographic.institution (
	id serial NOT NULL,
	institution_name varchar(1024) NOT NULL,
	id_time_zone int4 NOT NULL,
	CONSTRAINT institution_pk PRIMARY KEY (id),
	CONSTRAINT institution_un UNIQUE (institution_name),
	CONSTRAINT time_zone_fk FOREIGN KEY (id_time_zone) REFERENCES demographic.time_zone(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL
);

-- demographic.subject definition

-- Drop table

-- DROP TABLE demographic.subject;

CREATE TABLE demographic.subject (
	id_mined serial NOT NULL,
	dt_birth date NULL,
	note varchar(1024) NULL,
	id_subject int4 NOT NULL,
	id_institution int4 NOT NULL DEFAULT 1,
	seizure_onset_age float4 NULL,
	id_sex int2 NOT NULL DEFAULT 1,
	id_subject_type int4 NOT NULL DEFAULT 1,
	id_handedness int2 NOT NULL DEFAULT 1,
	CONSTRAINT mined_pk PRIMARY KEY (id_mined),
	CONSTRAINT subject_un UNIQUE (id_subject, id_institution),
	CONSTRAINT handedness_fk FOREIGN KEY (id_handedness) REFERENCES demographic.handedness(id) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT institution_fk FOREIGN KEY (id_institution) REFERENCES demographic.institution(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT sex_fk FOREIGN KEY (id_sex) REFERENCES demographic.sex(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT subject_type_fk FOREIGN KEY (id_subject_type) REFERENCES demographic.subject_type(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL
);


-- demographic.subject_histopathologies definition

-- Drop table

-- DROP TABLE demographic.subject_histopathologies;

CREATE TABLE demographic.subject_histopathologies (
	id serial NOT NULL,
	dt_date_utc timestamp NOT NULL,
	id_histopathology int4 NULL,
	id_mined_subject int4 NULL,
	CONSTRAINT subject_histopathologies_pk PRIMARY KEY (id),
	CONSTRAINT subject_histopathologies_un UNIQUE (dt_date_utc, id_histopathology, id_mined_subject),
	CONSTRAINT histopathology_fk FOREIGN KEY (id_histopathology) REFERENCES demographic.histopathology(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT subject_fk FOREIGN KEY (id_mined_subject) REFERENCES demographic.subject(id_mined) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL
);


-- demographic.subject_interventions definition

-- Drop table

-- DROP TABLE demographic.subject_interventions;

CREATE TABLE demographic.subject_interventions (
	id serial NOT NULL,
	dt_date_utc timestamp NOT NULL,
	id_intervention int4 NOT NULL,
	id_mined_subject int4 NOT NULL,
	CONSTRAINT subject_interventions_pk PRIMARY KEY (id),
	CONSTRAINT subject_interventions_un UNIQUE (dt_date_utc, id_intervention, id_mined_subject),
	CONSTRAINT intervention_fk FOREIGN KEY (id_intervention) REFERENCES demographic.intervention(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT subject_fk FOREIGN KEY (id_mined_subject) REFERENCES demographic.subject(id_mined) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL
);


-- demographic.subject_outcomes definition

-- Drop table

-- DROP TABLE demographic.subject_outcomes;

CREATE TABLE demographic.subject_outcomes (
	id serial NOT NULL,
	dt_date_utc timestamp NOT NULL,
	id_ilae_scale int4 NULL,
	id_engel_scale int4 NULL,
	id_mined_subject int4 NULL,
	CONSTRAINT outcome_pk PRIMARY KEY (id),
	CONSTRAINT outcome_un UNIQUE (dt_date_utc, id_mined_subject),
	CONSTRAINT engel_scale_fk FOREIGN KEY (id_engel_scale) REFERENCES demographic.engel_scale(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT ilae_scale_fk FOREIGN KEY (id_ilae_scale) REFERENCES demographic.ilae_scale(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT subject_fk FOREIGN KEY (id_mined_subject) REFERENCES demographic.subject(id_mined) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL
);


-- recording.modality definition

-- Drop table

-- DROP TABLE recording.modality;

CREATE TABLE recording.modality (
	id serial NOT NULL,
	modality_name varchar(1024) NOT NULL,
	CONSTRAINT modality_pk PRIMARY KEY (id),
	CONSTRAINT modality_un UNIQUE (modality_name)
);

-- recording.session_type definition

-- Drop table

-- DROP TABLE recordig.session_type;

CREATE TABLE recording.session_type (
	id serial NOT NULL,
	type_name varchar(1024) NOT NULL,
	CONSTRAINT session_type_pk PRIMARY KEY (id),
	CONSTRAINT session_type_un UNIQUE (type_name)
);



-- recording.task definition

-- Drop table

-- DROP TABLE recording.task;

CREATE TABLE recording.task (
	id serial NOT NULL,
	task_name varchar(1024) NOT NULL,
	task_description varchar(1024) NULL,
	patient_instructions varchar(1024) NULL,
	CONSTRAINT task_pk PRIMARY KEY (id),
	CONSTRAINT task_un UNIQUE (task_name)
);

-- recording.trial_type definition

-- Drop table

-- DROP TABLE recording.trial_type;

CREATE TABLE recording.trial_type (
	id serial NOT NULL,
	type_name varchar(1024) NOT NULL,
	id_task int4 NOT NULL,
	CONSTRAINT trial_type_pk PRIMARY KEY (id),
	CONSTRAINT trial_type_un UNIQUE (type_name, id_task),
	CONSTRAINT task_fk FOREIGN KEY (id_task) REFERENCES recording.task(id)
);


-- signal.channel_type definition

-- Drop table

-- DROP TABLE signal.channel_type;

CREATE TABLE signal.channel_type (
	id serial NOT NULL,
	type_name varchar(1024) NOT NULL,
	CONSTRAINT channel_type_fk PRIMARY KEY (id),
	CONSTRAINT channel_type_un UNIQUE (type_name)
);

-- signal.quality_type definition

-- Drop table

-- DROP TABLE signal.quality_type;

CREATE TABLE signal.quality_type (
	id serial NOT NULL,
	type_name varchar(1024) NOT NULL,
	CONSTRAINT quality_type_pk PRIMARY KEY (id),
	CONSTRAINT quality_type_un UNIQUE (type_name)
);


-- signal.recording_reference definition

-- Drop table

-- DROP TABLE signal.recording_reference;

CREATE TABLE signal.recording_reference (
	id serial NOT NULL,
	reference_name varchar(1024) NOT NULL,
	reference_note varchar(1024) NULL,
	CONSTRAINT recording_reference_pk PRIMARY KEY (id),
	CONSTRAINT recording_reference_un UNIQUE (reference_name)
);

-- signal.seizure_channel_type definition

-- Drop table

-- DROP TABLE signal.seizure_channel_type;

CREATE TABLE signal.seizure_channel_type (
	id bigserial NOT NULL,
	type_name varchar(128) NOT NULL,
	CONSTRAINT seizure_channel_type_pk PRIMARY KEY (id),
	CONSTRAINT seizure_channel_type_un UNIQUE (type_name)
);


-- signal.seizure_type definition

-- Drop table

-- DROP TABLE signal.seizure_type;

CREATE TABLE signal.seizure_type (
	id bigserial NOT NULL,
	type_name varchar(256) NOT NULL,
	CONSTRAINT seizure_type_pk PRIMARY KEY (id),
	CONSTRAINT seizure_type_un UNIQUE (type_name)
);



-- "space".anatomy_structure definition

-- Drop table

-- DROP TABLE "space".anatomy_structure;

CREATE TABLE "space".anatomy_structure (
	id serial NOT NULL,
	structure_name varchar(1024) NULL,
	CONSTRAINT anatomy_structure_pk PRIMARY KEY (id),
	CONSTRAINT anatomy_structure_un UNIQUE (structure_name)
);


-- "space".atlas_type definition

-- Drop table

-- DROP TABLE "space".atlas_type;

CREATE TABLE "space".atlas_type (
	id serial NOT NULL,
	type_name varchar(1024) NOT NULL,
	CONSTRAINT atlas_type_pk PRIMARY KEY (id),
	CONSTRAINT atlas_type_un UNIQUE (type_name)
);



-- "space".contact_size definition

-- Drop table

-- DROP TABLE "space".contact_size;

CREATE TABLE "space".contact_size (
	id serial NOT NULL,
	size_name varchar(1024) NOT NULL,
	size_value float4 NOT NULL DEFAULT '-1'::integer,
	CONSTRAINT contact_size_pk PRIMARY KEY (id)
);


-- "space".contact_type definition

-- Drop table

-- DROP TABLE "space".contact_type;

CREATE TABLE "space".contact_type (
	id serial NOT NULL,
	type_name varchar(1024) NOT NULL,
	CONSTRAINT contact_type_pk PRIMARY KEY (id),
	CONSTRAINT contact_type_un UNIQUE (type_name)
);


-- "space".coordinate_system definition

-- Drop table

-- DROP TABLE "space".coordinate_system;

CREATE TABLE "space".coordinate_system (
	id serial NOT NULL,
	system_name varchar(1024) NOT NULL,
	system_unit varchar(32) NOT NULL,
	CONSTRAINT coordinate_system_pk PRIMARY KEY (id),
	CONSTRAINT coordinate_system_un UNIQUE (system_name, system_unit)
);


-- "space".electrode_type definition

-- Drop table

-- DROP TABLE "space".electrode_type;

CREATE TABLE "space".electrode_type (
	id serial NOT NULL,
	type_name varchar(128) NOT NULL,
	manufacturer varchar(1024) NOT NULL DEFAULT 'n/a'::character varying,
	contact_material varchar(1024) NULL,
	macro_contact_distance float4 NULL,
	micro_contact_distance float4 NULL,
	dimension_a int2 NOT NULL DEFAULT '-1'::integer,
	dimension_b int2 NOT NULL DEFAULT '-1'::integer,
	CONSTRAINT electrode_type_pk PRIMARY KEY (id),
	CONSTRAINT electrode_type_un UNIQUE (type_name, manufacturer, dimension_a, dimension_b)
);

-- "space".imaging_pathology definition

-- Drop table

-- DROP TABLE "space".imaging_pathology;

CREATE TABLE "space".imaging_pathology (
	id serial NOT NULL,
	pathology_name varchar NOT NULL,
	CONSTRAINT imaging_pathology_pk PRIMARY KEY (id)
);


-- "space".lobe definition

-- Drop table

-- DROP TABLE "space".lobe;

CREATE TABLE "space".lobe (
	id serial NOT NULL,
	lobe_name varchar(1024) NULL,
	CONSTRAINT lobe_pk PRIMARY KEY (id),
	CONSTRAINT lobe_un UNIQUE (lobe_name)
);

-- util.schema_changes definition

-- Drop table

-- DROP TABLE util.schema_changes;

CREATE TABLE util.schema_changes (
	id serial NOT NULL,
	major_release_number varchar(2) NOT NULL,
	minor_release_number varchar(2) NOT NULL,
	point_release_number varchar(4) NOT NULL,
	script_name varchar(64) NOT NULL,
	date_applied timestamp NOT NULL DEFAULT now(),
	CONSTRAINT schema_changes_pk PRIMARY KEY (id),
	CONSTRAINT schema_changes_un UNIQUE (major_release_number, minor_release_number, point_release_number)
);


-- "space".anatomy definition

-- Drop table

-- DROP TABLE "space".anatomy;

CREATE TABLE "space".anatomy (
	id serial NOT NULL,
	hemisphere bool NOT NULL,
	brain_area int2 NOT NULL DEFAULT '-1'::integer,
	id_atlas_type int4 NOT NULL,
	id_anatomy_structure int4 NOT NULL,
	id_lobe int4 NOT NULL,
	CONSTRAINT anatomy_pk PRIMARY KEY (id),
	CONSTRAINT anatomy_un UNIQUE (hemisphere, id_atlas_type, id_anatomy_structure, id_lobe),
	CONSTRAINT anatomy_structure_fk FOREIGN KEY (id_anatomy_structure) REFERENCES "space".anatomy_structure(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT atlas_type_fk FOREIGN KEY (id_atlas_type) REFERENCES "space".atlas_type(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT lobe_fk FOREIGN KEY (id_lobe) REFERENCES "space".lobe(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL
);

COMMENT ON COLUMN "space".anatomy.hemisphere IS 'true is right, false is left';

--- table with relationships between schemas--------

-- recording."session" definition : recording,signal, demographic

-- Drop table

-- DROP TABLE recording."session";

CREATE TABLE recording."session" (
	id bigserial NOT NULL,
	dt_start_utc timestamp NOT NULL,
	dt_stop_utc timestamp NOT NULL,
	acquisition_device varchar(128) NULL,
	ip_address varchar(64) NULL,
	session_name varchar(1024) NOT NULL,
	note varchar(2018) NULL,
	id_session_type int4 NOT NULL DEFAULT 1,
	id_modality int4 NOT NULL DEFAULT 1,
	id_recording_reference int4 NOT NULL DEFAULT 1,
	id_mined_subject int4 NOT NULL,
	CONSTRAINT session_pk PRIMARY KEY (id),
	CONSTRAINT session_un UNIQUE (dt_start_utc, id_mined_subject),
	CONSTRAINT modality_fk FOREIGN KEY (id_modality) REFERENCES recording.modality(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT recording_reference_fk FOREIGN KEY (id_recording_reference) REFERENCES signal.recording_reference(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT session_type_fk FOREIGN KEY (id_session_type) REFERENCES recording.session_type(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT subject_fk FOREIGN KEY (id_mined_subject) REFERENCES demographic.subject(id_mined) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL
);


-- "space".electrode definition

-- Drop table

-- DROP TABLE "space".electrode;

CREATE TABLE "space".electrode (
	id serial NOT NULL,
	electrode_name varchar(1024) NOT NULL,
	id_electrode_type int4 NOT NULL DEFAULT 1,
	id_mined_subject int8 NOT NULL,
	CONSTRAINT electrode_pk PRIMARY KEY (id),
	CONSTRAINT electrode_un UNIQUE (electrode_name, id_electrode_type, id_mined_subject),
	CONSTRAINT electrode_type_fk FOREIGN KEY (id_electrode_type) REFERENCES "space".electrode_type(id) ON UPDATE CASCADE ON DELETE RESTRICT,
	CONSTRAINT subject_fk FOREIGN KEY (id_mined_subject) REFERENCES "demographic".subject(id_mined) ON UPDATE CASCADE ON DELETE RESTRICT
);


-- "space".contact definition

-- Drop table

-- DROP TABLE "space".contact;

CREATE TABLE "space".contact (
	id bigserial NOT NULL,
	contact_number int4 NOT NULL,
	impedance float8 NULL,
	id_contact_size int4 NOT NULL DEFAULT 1,
	id_contact_type int4 NOT NULL DEFAULT 1,
	id_electrode int4 NULL,
	id_imaging_pathology int4 NOT NULL DEFAULT 1,
	CONSTRAINT contact_pk PRIMARY KEY (id),
	CONSTRAINT contact_un UNIQUE (contact_number, id_electrode),
	CONSTRAINT contact_size_fk FOREIGN KEY (id_contact_size) REFERENCES "space".contact_size(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT contact_type_fk FOREIGN KEY (id_contact_type) REFERENCES "space".contact_type(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT electrode_fk FOREIGN KEY (id_electrode) REFERENCES "space".electrode(id) ON UPDATE CASCADE ON DELETE RESTRICT,
	CONSTRAINT imaging_pathology_fk FOREIGN KEY (id_imaging_pathology) REFERENCES "space".imaging_pathology(id) ON UPDATE CASCADE ON DELETE SET NULL
);


-- "space".contact_subjects_interventions definition

-- Drop table

-- DROP TABLE "space".contact_subjects_interventions;

CREATE TABLE "space".contact_subjects_interventions (
	id bigserial NOT NULL,
	id_contact int8 NOT NULL,
	id_subject_intervention int4 NOT NULL,
	CONSTRAINT contact_subject_interventions_pk PRIMARY KEY (id),
	CONSTRAINT contact_subject_interventions_un UNIQUE (id_contact, id_subject_intervention),
	CONSTRAINT contact_fk FOREIGN KEY (id_contact) REFERENCES "space".contact(id) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT subject_interventions_fk FOREIGN KEY (id_subject_intervention) REFERENCES demographic.subject_interventions(id) ON UPDATE CASCADE ON DELETE SET NULL
);


-- "space".coordinates definition

-- Drop table

-- DROP TABLE "space".coordinates;

CREATE TABLE "space".coordinates (
	id serial NOT NULL,
	c_x float4 NULL,
	c_y float4 NULL,
	c_z float4 NULL,
	id_coordinate_system int4 NULL,
	id_contact int8 NOT NULL,
	CONSTRAINT coordinates_pk PRIMARY KEY (id),
	CONSTRAINT coordinates_un UNIQUE (id_contact, id_coordinate_system),
	CONSTRAINT coordinate_system_fk FOREIGN KEY (id_coordinate_system) REFERENCES "space".coordinate_system(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT coordinates_fk FOREIGN KEY (id_contact) REFERENCES "space".contact(id) ON UPDATE CASCADE ON DELETE SET DEFAULT
);

-- "space".anatomy_contact definition

-- Drop table

-- DROP TABLE "space".anatomy_contact;

CREATE TABLE "space".anatomy_contact (
	id bigserial NOT NULL,
	id_contact int8 NOT NULL,
	id_anatomy int4 NOT NULL,
	manual_assign bool NOT NULL,
	CONSTRAINT anatomy_contact_pk PRIMARY KEY (id),
	CONSTRAINT anatomy_contact_un UNIQUE (id_contact, id_anatomy, manual_assign),
	CONSTRAINT anatomy_fk FOREIGN KEY (id_anatomy) REFERENCES "space".anatomy(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT contact_fk FOREIGN KEY (id_contact) REFERENCES "space".contact(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL
);


-- recording.trial definition

-- Drop table

-- DROP TABLE recording.trial;

CREATE TABLE recording.trial (
	id serial NOT NULL,
	onset int4 NOT NULL,
	duration int4 NOT NULL,
	response_time int4 NULL,
	response varchar(64) NULL,
	ttl_value int4 NULL,
	id_trial_type int4 NOT NULL,
	id_session int8 NOT NULL,
	CONSTRAINT trial_pk PRIMARY KEY (id),
	CONSTRAINT trial_un UNIQUE (onset, id_trial_type, id_session),
	CONSTRAINT session_fk FOREIGN KEY (id_session) REFERENCES recording.session(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT trial_type_fk FOREIGN KEY (id_trial_type) REFERENCES recording.trial_type(id)
);

-- recording.annotation_session definition

-- Drop table

-- DROP TABLE recording.annotation_session;

CREATE TABLE recording.annotation_session (
	id bigserial NOT NULL,
	event_start_utc timestamp NOT NULL,
	event_stop_utc timestamp NULL,
	id_session int8 NOT NULL,
	annotation_text varchar(256) NULL,
	CONSTRAINT annotation_session_pk PRIMARY KEY (id),
	CONSTRAINT annotation_session_un UNIQUE (event_start_utc, event_stop_utc, id_session),
	CONSTRAINT session_fk FOREIGN KEY (id_session) REFERENCES recording.session(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL
);



-- util."_bids_entities" definition : util,recording

-- Drop table

-- DROP TABLE util."_bids_entities";

CREATE TABLE util."_bids_entities" (
	id int8 NOT NULL,
	bids_subject varchar(16) NOT NULL,
	bids_session varchar(16) NULL,
	bids_modality varchar(16) NOT NULL,
	bids_task varchar(16) NOT NULL,
	bids_run int2 NULL,
	CONSTRAINT "_bids_entities_pk" PRIMARY KEY (id),
	CONSTRAINT "_bids_entities_un" UNIQUE (bids_subject, bids_session, bids_modality, bids_task, bids_run),
	CONSTRAINT "_bids_entities_fk" FOREIGN KEY (id) REFERENCES recording.session(id) ON UPDATE CASCADE ON DELETE SET NULL
);


-- signal.channel definition : signal, recording

-- Drop table

-- DROP TABLE signal.channel;

CREATE TABLE signal.channel (
	id bigserial NOT NULL,
	channel_name varchar(1024) NULL,
	sampling_frequency float4 NOT NULL,
	id_quality_type int4 NOT NULL DEFAULT 1,
	id_channel_type int4 NOT NULL DEFAULT 1,
	id_session int8 NOT NULL,
	low_cutoff float4 NOT NULL DEFAULT '-1'::integer,
	high_cutoff float4 NOT NULL DEFAULT '-1'::integer,
	unit varchar(4) NOT NULL DEFAULT 'n/a'::character varying,
	acquisition_number int4 NULL,
	CONSTRAINT channel_pk PRIMARY KEY (id),
	CONSTRAINT channel_un UNIQUE (channel_name, id_session),
	CONSTRAINT channel_fk FOREIGN KEY (id_session) REFERENCES recording.session(id) ON UPDATE CASCADE ON DELETE RESTRICT,
	CONSTRAINT channel_type_fk FOREIGN KEY (id_channel_type) REFERENCES signal.channel_type(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT quality_type_fk FOREIGN KEY (id_quality_type) REFERENCES signal.quality_type(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL
);

-- signal.annotation_channel definition

-- Drop table

-- DROP TABLE signal.annotation_channel;

CREATE TABLE signal.annotation_channel (
	id bigserial NOT NULL,
	event_start_utc timestamp NOT NULL,
	event_stop_utc timestamp NULL,
	annotation_text varchar(256) NULL,
	id_channel int8 NOT NULL,
	CONSTRAINT annotation_channel_pk PRIMARY KEY (id),
	CONSTRAINT annotation_channel_un UNIQUE (event_start_utc, event_stop_utc, id_channel),
	CONSTRAINT channel_fk FOREIGN KEY (id_channel) REFERENCES signal.channel(id) ON UPDATE CASCADE ON DELETE CASCADE
);

-- signal.seizure definition

-- Drop table

-- DROP TABLE signal.seizure;

CREATE TABLE signal.seizure (
	id bigserial NOT NULL,
	id_seizure_channel_type int8 NULL,
	id_seizure_type int8 NULL,
	id_annotation_channel int8 NULL,
	CONSTRAINT seisure_pk PRIMARY KEY (id),
	CONSTRAINT seizure_un UNIQUE (id_seizure_channel_type, id_annotation_channel),
	CONSTRAINT annotation_channel_fk FOREIGN KEY (id_annotation_channel) REFERENCES signal.annotation_channel(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT seizure_channel_type_fk FOREIGN KEY (id_seizure_channel_type) REFERENCES signal.seizure_channel_type(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT seizure_type_fk FOREIGN KEY (id_seizure_type) REFERENCES signal.seizure_type(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL
);

-- signal.channel_contact definition : signal, space

-- Drop table

-- DROP TABLE signal.channel_contact;

CREATE TABLE signal.channel_contact (
	id bigserial NOT NULL,
	id_channel int8 NOT NULL,
	id_contact int8 NOT NULL,
	id_recording_reference int4 NOT NULL,
	CONSTRAINT channel_contact_pk PRIMARY KEY (id),
	CONSTRAINT channel_contact_un UNIQUE (id_channel, id_contact, id_recording_reference),
	CONSTRAINT channel_fk FOREIGN KEY (id_channel) REFERENCES signal.channel(id) ON UPDATE CASCADE ON DELETE RESTRICT,
	CONSTRAINT contact_fk FOREIGN KEY (id_contact) REFERENCES "space".contact(id) ON UPDATE CASCADE ON DELETE RESTRICT,
	CONSTRAINT recording_reference_fk FOREIGN KEY (id_recording_reference) REFERENCES signal.recording_reference(id) ON UPDATE CASCADE ON DELETE RESTRICT
);


