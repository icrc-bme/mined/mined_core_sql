--Timezone - creates a partial copy from PostgreSQL system table
DELETE FROM demographic.time_zone;
ALTER SEQUENCE demographic.time_zone_id_seq RESTART WITH 1;
INSERT INTO demographic.time_zone (zone_name) SELECT name FROM pg_timezone_names WHERE name NOT LIKE 'posix%';

--Histopathology
DELETE FROM demographic.histopathology;
ALTER SEQUENCE demographic.histopathology_id_seq RESTART WITH 1;
INSERT INTO demographic.histopathology (histopathology_name) values ('n/a'), ('unknown'), ('negative'), ('HS type 1'), ('HS type 2'), ('HS type 3'), ('FCD 1a'), ('FCD 1b'), ('FCD 2a'), ('FCD 2b'), ('FCD 3a'), ('FCD 3b'), ('FCD 3c'), ('gliosis');

--Intervention
DELETE FROM demographic.intervention;
ALTER SEQUENCE demographic.intervention_id_seq RESTART WITH 1;
INSERT INTO demographic.intervention(intervention_name) values ('n/a'), ('unknown'), ('VNS'), ('lesionectomy'), ('cortectopy'), ('thermocoagulation'), ('AMTR');

--Institution	-----------------C H A N G E -------------------------------
DELETE FROM demographic.institution;
ALTER SEQUENCE demographic.institution_id_seq RESTART WITH 1;
--INSERT INTO dbo.institution (institution_name) VALUES ('n/a');
DO $$
DECLARE id_GMT int;
BEGIN
	SELECT INTO id_GMT id FROM demographic.time_zone WHERE zone_name='GMT';
INSERT INTO demographic.institution (institution_name, id_time_zone ) VALUES ('n/a', id_GMT);
END $$;
--------------------------------------------------------------------------
--Engel scale
DELETE FROM demographic.engel_scale;
ALTER SEQUENCE demographic.engel_scale_id_seq RESTART WITH 1;
INSERT INTO demographic.engel_scale (scale_name) VALUES ('n/a'), ('I'), ('IA'), ('IB'), ('IC'), ('ID'), ('II'), ('IIA'), ('IIB'), ('IIC'), ('IID'), ('III'), ('IIIA'), ('IIIB'), ('IV'), ('IVA'), ('IVB'), ('IVC');
    
--ILAE scale
DELETE FROM demographic.ilae_scale;
ALTER SEQUENCE demographic.ilae_scale_id_seq RESTART WITH 1;
INSERT INTO demographic.ilae_scale (scale_name) VALUES ('n/a'), ('Class 1'), ('Class 2'), ('Class 3'), ('Class 4'), ('Class 5'), ('Class 6');

--Sex
DELETE FROM demographic.sex;
ALTER SEQUENCE demographic.sex_id_seq RESTART WITH 1;
INSERT INTO demographic.sex (sex_name) VALUES ('n/a'), ('male'), ('female'), ('unspecified');

--Subject type
DELETE FROM demographic.subject_type;
ALTER SEQUENCE demographic.subject_type_id_seq RESTART WITH 1;
INSERT INTO demographic.subject_type (type_name) VALUES ('n/a'), ('human'), ('primate'), ('pig'), ('dog'), ('rat');

--Handedness
DELETE FROM demographic.handedness;
ALTER SEQUENCE demographic.handedness_id_seq RESTART WITH 1;
INSERT INTO demographic.handedness (handedness_name) VALUES ('n/a'), ('left'), ('right'), ('ambidextrous');


--Modality
DELETE FROM recording.modality;
ALTER SEQUENCE recording.modality_id_seq RESTART WITH 1;
INSERT INTO recording.modality (modality_name) VALUES ('n/a'), ('EEG'), ('iEEG'), ('VIDEO');

--Session type
DELETE FROM recording.session_type;
ALTER SEQUENCE recording.session_type_id_seq RESTART WITH 1;
INSERT INTO recording.session_type (type_name) VALUES ('n/a'), ('clinic'), ('research');

--Task
DELETE FROM recording.task;
ALTER SEQUENCE recording.task_id_seq RESTART WITH 1;
INSERT INTO recording.task (task_name) VALUES ('n/a');

--Trial type
DELETE FROM recording.trial_type;
ALTER SEQUENCE recording.trial_type_id_seq RESTART WITH 1;
INSERT INTO recording.trial_type (type_name, id_task) VALUES ('n/a', 1);

--Quality type
DELETE FROM signal.quality_type;
ALTER SEQUENCE signal.quality_type_id_seq RESTART WITH 1;
INSERT INTO signal.quality_type (type_name) VALUES ('n/a'), ('good'), ('medium'), ('bad'), ('unusable');

--Channel type - taken from BIDS (https://bids-specification.readthedocs.io/en/stable/04-modality-specific-files/04-intracranial-electroencephalography.html)
DELETE FROM signal.channel_type;
ALTER SEQUENCE signal.channel_type_id_seq RESTART WITH 1;
INSERT INTO signal.channel_type (type_name) VALUES ('n/a'), ('EEG'), ('ECOG'), ('SEEG'), ('DBS'), ('VEOG'), ('HEOG'), ('EOG'), ('ECG'), ('EMG'), ('TRIG'), ('AUDIO'), ('PD'), ('EYEGAZE'), ('PUPIL'), ('MISC'), ('SYSCLOCK'), ('ADC'), ('DAC'), ('REF'), ('OTHER');

--Recording referenece
DELETE FROM signal.recording_reference;
ALTER SEQUENCE signal.recording_reference_id_seq RESTART WITH 1;
INSERT INTO signal.recording_reference (reference_name) VALUES ('n/a');

--Seizure type
DELETE FROM signal.seizure_type;
ALTER SEQUENCE signal.seizure_type_id_seq RESTART WITH 1;
INSERT INTO signal.seizure_type(type_name) values ('n/a'), ('unknown'), ('focal'), ('generalized'), ('propagated'), ('mixed');

--Seizure channel type
DELETE FROM signal.seizure_channel_type;
ALTER SEQUENCE signal.seizure_channel_type_id_seq RESTART WITH 1;
INSERT INTO signal.seizure_channel_type(type_name) values ('n/a'), ('unknown'), ('seizure onset zone'), ('irritative zone'), ('early propagation'), ('normal');

--Contact size
DELETE FROM "space".contact_size;
ALTER SEQUENCE "space".contact_size_id_seq RESTART WITH 1;
INSERT INTO "space".contact_size (size_name) VALUES ('n/a'), ('macro'), ('micro');

--Coordinate system
DELETE FROM "space".coordinate_system;
ALTER SEQUENCE "space".coordinate_system_id_seq RESTART WITH 1;
INSERT INTO "space".coordinate_system (system_name, system_unit) VALUES ('n/a', 'n/a'), ('MNI305', 'mm');

--Contact type
DELETE FROM "space".contact_type;
ALTER SEQUENCE "space".contact_type_id_seq RESTART WITH 1;
INSERT INTO "space".contact_type (type_name) VALUES ('n/a'), ('other'), ('ring'), ('disk'), ('wire'), ('bundle'), ('shaft'), ('cup');

--Atlast type
DELETE FROM "space".atlas_type;
ALTER SEQUENCE "space".atlas_type_id_seq RESTART WITH 1;
INSERT INTO "space".atlas_type (type_name) VALUES ('n/a'), ('Talairach'), ('MNI305');

--Anatomy structure
DELETE FROM "space".anatomy_structure;
ALTER SEQUENCE "space".anatomy_structure_id_seq RESTART WITH 1;
INSERT INTO "space".anatomy_structure (structure_name) VALUES ('n/a');

--Lobe
DELETE FROM "space".lobe;
ALTER SEQUENCE "space".lobe_id_seq RESTART WITH 1;
INSERT INTO "space".lobe (lobe_name) VALUES ('n/a');

--Imaging pathology
DELETE FROM "space".imaging_pathology;
ALTER SEQUENCE "space".imaging_pathology_id_seq RESTART WITH 1;
INSERT INTO "space".imaging_pathology (pathology_name) VALUES
 ('n/a'),
 ('none'),
 ('lesion'),
 ('postoperation pseudocyst'),
 ('gliosis'),
 ('postoperation defect'),
 ('nodular heterotopia'),
 ('malformation of cortical development'),
 ('residual cortex');

--Electrode type
DELETE FROM "space".electrode_type;
ALTER SEQUENCE "space".electrode_type_id_seq RESTART WITH 1;
INSERT INTO
	"space".electrode_type (type_name,
						manufacturer,
						contact_material,
						macro_contact_distance,
						micro_contact_distance,
						dimension_a,
						dimension_b)
VALUES
	('n/a',
	 'n/a',
	 'n/a',
	 0,
	 0,
	 0,
	 0);

