CREATE OR REPLACE FUNCTION demographic.delete_seizure_channel(id_annotation_channel integer, 
					OUT id integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$

-- seizure is special kind of annotation 

		
BEGIN
	DELETE FROM demographic.seizure WHERE dbo.seizure.id_annotation_channel = $1;
	DELETE FROM demographic.annotation_channel WHERE demographic.annotation_channel.id = $1;
	$2 = 1;
	
	RETURN;
END
$function$
;
