CREATE OR REPLACE FUNCTION "space".insert_contact_subject_interventions(
	IN id_subject_intervention integer,
	IN id_contact integer,
    OUT id integer)
  RETURNS integer AS
$BODY$

DECLARE 

BEGIN

	INSERT INTO "space".contact_subjects_interventions(
		id_subject_intervention,
		id_contact) 
	VALUES (
		$1,
		$2)
	RETURNING
		"space".contact_subjects_interventions.id INTO $3;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;