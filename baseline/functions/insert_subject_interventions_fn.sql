CREATE OR REPLACE FUNCTION demographic.insert_subject_interventions(
    IN dt_date_utc TIMESTAMP,
    IN id_mined integer,
    IN intervention_name character varying,
    OUT id_subject_intervention integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalog indices
	idx_intervention		INTEGER default 1;

BEGIN

	--- Get intervention id
	IF intervention_name IS NOT NULL THEN
		SELECT INTO idx_intervention demographic.intervention.id FROM demographic.intervention WHERE demographic.intervention.intervention_name = $3;
		IF idx_intervention IS NULL THEN
			INSERT INTO demographic.intervention(intervention_name) VALUES ($3) RETURNING id INTO idx_intervention;
		END IF;
	END IF;

	INSERT INTO demographic.subject_interventions(
		dt_date_utc,
		id_mined_subject,
		id_intervention)
	VALUES (
		$1,
		$2,
		idx_intervention)
	RETURNING
		demographic.subject_interventions.id INTO $4;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 
