CREATE OR REPLACE FUNCTION signal.save_annotation_channel(
	IN insert_annotation_channel boolean, 
	IN dt_start timestamp without time zone,
	IN dt_stop timestamp without time zone,								
	IN annotation_text character varying,
	IN ida_channel bigint[],
	OUT success integer)
RETURNS integer AS
$BODY$


DECLARE 

	--- Temporary values
	idx_channel			INTEGER;
		
BEGIN	

	BEGIN
		IF insert_annotation_channel THEN
			FOREACH idx_channel IN ARRAY ida_channel
			LOOP
				INSERT INTO signal.annotation_channel (
					event_start_utc,
					event_stop_utc,
					annotation_text,
					id_channel) VALUES ($2, $3, $4, idx_channel); 
			END LOOP;
			$6 = 1;
		ELSE
			FOREACH idx_channel IN ARRAY ida_channel
			LOOP
				UPDATE signal.annotation_channel SET 
					event_start_utc = $2,
					event_stop_utc= $3,
					annotation_text = $4
				WHERE signal.annotation_channel.id = idx_channel;
			END LOOP;
			$6 = 2;
		END IF;
	EXCEPTION WHEN OTHERS THEN
		$6 = -255;
	END;
	$6 = 1;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
