CREATE OR REPLACE FUNCTION recording.save_annotation_session(
	IN id_annotation_session integer,
	IN event_start_utc TIMESTAMP,
    IN id_session integer,
    IN event_stop_utc TIMESTAMP,
    IN annotation_text character varying,
    OUT id_annotation integer)
 RETURNS integer AS
$BODY$

DECLARE 

BEGIN	

	BEGIN
		IF $1 = 0 THEN
			INSERT INTO recording.annotation_session(
				event_start_utc,
				id_session,
				event_stop_utc,
				annotation_text) VALUES( $2, $3, $4, $5) RETURNING recording.annotation_session.id INTO $6;
		ELSE
			UPDATE recording.annotation_session SET 
				event_start_utc = $2,
				id_session = $3,
				event_stop_utc = $4,
				annotation_text = $5
			WHERE  recording.annotation_session.id = $1;
			$6 = $1;
		END IF;
	EXCEPTION WHEN OTHERS THEN
		$6 = -255;
	END;

	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

