CREATE OR REPLACE FUNCTION "space".save_contact(
		IN id_contact integer,
    IN contact_number integer,
	  IN id_electrode integer,
	  IN impedance float,
    IN contact_size character varying,
    IN contact_size_value float,
    IN contact_type character varying,
    IN imaging_pathology character varying,
    OUT out_id_contact integer)
  RETURNS integer AS
$BODY$


DECLARE
--- Catalogs
	idx_contact_size			INTEGER default 1;
	idx_contact_type			INTEGER default 1;
	idx_imaging_pathology			INTEGER default 1;

BEGIN

	--- Get contact size id
	IF contact_size IS NOT NULL THEN
		SELECT INTO idx_contact_size "space".contact_size.id FROM "space".contact_size WHERE "space".contact_size.size_name = $5 AND "space".contact_size.size_value = $6;
		IF idx_contact_size IS NULL THEN
			$9 = -901;
			RETURN;
		END IF;
	END IF;

	--- Get contact type id
	IF contact_type IS NOT NULL THEN
		SELECT INTO idx_contact_type "space".contact_type.id FROM "space".contact_type WHERE "space".contact_type.type_name = $7;
		IF idx_contact_type IS NULL THEN
			$9 = -902;
			RETURN;
		END IF;
	END IF;

	--- Get imaging pathology id
	IF imaging_pathology IS NOT NULL THEN
		SELECT INTO idx_imaging_pathology "space".imaging_pathology.id FROM "space".imaging_pathology WHERE "space".imaging_pathology.pathology_name = $8;
		IF idx_imaging_pathology IS NULL THEN
			$9 = -903;
			RETURN;
		END IF;
	END IF;


	BEGIN
	IF $1 = 0 THEN
		INSERT INTO "space".contact(
			contact_number,
			id_electrode,
			impedance,
			id_contact_size,
			id_contact_type,
			id_imaging_pathology
			) 
		VALUES (
			$2,
			$3,
			$4,
			idx_contact_size,
			idx_contact_type,
			idx_imaging_pathology
			)
		RETURNING
			"space".contact.id INTO $9;
	ELSE
		UPDATE "space".contact SET 
			contact_number = $2,
			id_electrode = $3,
			impedance = $4,
			id_contact_size = idx_contact_size,
			id_contact_type = idx_contact_type,
			id_imaging_pathology = idx_imaging_pathology
		WHERE  "space".contact.id = $1;
		$9 = $1;
	END IF;
	EXCEPTION WHEN OTHERS THEN
		$8 = -255;
	END;
	
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
