CREATE OR REPLACE FUNCTION demographic.save_outcome(
	id_outcome INTEGER,
	dt_date_utc TIMESTAMP,
	id_mined INTEGER,
	scale_name_engel character varying,
	scale_name_ilae character varying,
	OUT out_id_outcom INTEGER)
RETURNS INTEGER AS
$BODY$

DECLARE 

	--- Catalog indices
	idx_engel				INTEGER DEFAULT 1;
	idx_ilae				INTEGER DEFAULT 1;

BEGIN	

	--- Get engel scale id
	SELECT INTO idx_engel demographic.engel_scale.id FROM demographic.engel_scale WHERE demographic.engel_scale.scale_name = $4;
	IF idx_engel = 0 OR idx_engel IS NULL THEN
		$6 = -401;
		RETURN;
	END IF;

	--- Get ilae scale id
	SELECT INTO idx_ilae demographic.ilae_scale.id FROM demographic.ilae_scale WHERE demographic.ilae_scale.scale_name = $5;
	IF idx_ilae = 0 OR idx_ilae IS NULL THEN
		$6 = -402;
		RETURN;
	END IF;

	BEGIN
		IF $1 = 0 THEN
			INSERT INTO demographic.subject_outcomes( dt_date_utc, id_mined_subject, id_ilae_scale, id_engel_scale) VALUES( $2, $3, idx_ilae, idx_engel) RETURNING demographic.subject_outcomes.id INTO $6;
		ELSE
			UPDATE demographic.subject_outcomes SET dt_date_utc = $2, id_ilae_scale = idx_ilae, id_engel_scale= idx_engel WHERE demographic.subject_outcomes.id = $1;
			$6 = $1;
		END IF;
	EXCEPTION WHEN OTHERS THEN
		$6 = -255;
	END;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
