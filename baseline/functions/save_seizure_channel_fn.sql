CREATE OR REPLACE FUNCTION signal.save_seizure_channel(
	IN insert_seizure boolean, 
	IN dt_start timestamp without time zone,
	IN dt_stop timestamp without time zone,
	IN annotation_text character varying,
	IN ida_channel bigint[],
	IN seizure_type character varying,
	IN seizure_channel_type character varying,
	OUT success integer)
RETURNS integer AS
$BODY$

-- seizure is special kind of annotation 

DECLARE 

	--- Catalogs
	idx_seizure_type			INTEGER default 1;
	idx_seizure_channel_type	INTEGER default 1;

	--- Temporary values
	idx_channel			INTEGER;
	idx_annotation		INTEGER;
		
BEGIN	

	--- Get seizure type id
	IF seizure_type IS NOT NULL THEN
		SELECT INTO idx_seizure_type signal.seizure_type.id FROM signal.seizure_type WHERE signal.seizure_type.type_name = $6;
		IF idx_seizure_type IS NULL THEN
			$8 = -1301;
			RETURN;
		END IF;
	END IF;

	IF seizure_channel_type IS NOT NULL THEN
		SELECT INTO idx_seizure_channel_type signal.seizure_channel_type.id FROM signal.seizure_channel_type WHERE signal.seizure_channel_type.type_name = $7;
		IF idx_seizure_channel_type IS NULL THEN
			$8 = -1302;
			RETURN;
		END IF;
	END IF;

	BEGIN
		IF insert_seizure THEN
			FOREACH idx_channel IN ARRAY ida_channel
			LOOP
				INSERT INTO signal.annotation_channel (event_start_utc , event_stop_utc, annotation_text, id_channel )
						VALUES ($2, $3, $4, idx_channel)  RETURNING signal.annotation_channel.id INTO idx_annotation;
				INSERT INTO signal.seizure (id_annotation_channel , id_seizure_channel_type , id_seizure_type ) 
						VALUES (idx_annotation, idx_seizure_channel_type, idx_seizure_type);
			
			END LOOP;
		ELSE
			FOREACH idx_channel IN ARRAY ida_channel
			LOOP
				UPDATE signal.annotation_channel SET event_start_utc = $2, event_stop_utc= $3, annotation_text = $4 
						WHERE signal.annotation_channel.id = idx_channel;
				UPDATE signal.seizure SET id_seizure_type = idx_seizure_type, id_seizure_channel_type = idx_seizure_channel_type WHERE signal.seizure.id_annotation_channel = idx_channel;  
			END LOOP;
		END IF;
		
	EXCEPTION WHEN OTHERS THEN
		$8 = -255;
	END;
	$8 = 1;
	
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
