CREATE OR REPLACE FUNCTION "space".upsert_contact(
	IN contact_number integer,
	IN id_electrode integer,
	IN impedance float,
  IN contact_size character varying,
  IN contact_size_value float,
  IN contact_type character varying,
  IN imaging_pathology character varying,
    OUT id integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalogs
	idx_contact_size			INTEGER default 1;
	idx_contact_type			INTEGER default 1;
	idx_imaging_pathology INTEGER default 1;

BEGIN

	--- Get contact size id
	IF contact_size IS NOT NULL THEN
		SELECT INTO idx_contact_size "space".contact_size.id FROM "space".contact_size WHERE "space".contact_size.size_name = $4 AND "space".contact_size.size_value = $5;
		IF idx_contact_size IS NULL THEN
			INSERT INTO "space".contact_size(size_name, size_value) VALUES ($4, $5) RETURNING "space".contact_size.id INTO idx_contact_size;
		END IF;
	END IF;

	--- Get contact size id
	IF contact_type IS NOT NULL THEN
		SELECT INTO idx_contact_type "space".contact_type.id FROM "space".contact_type WHERE "space".contact_type.type_name = $6;
		IF idx_contact_type IS NULL THEN
			INSERT INTO "space".contact_type(type_name) VALUES ($6) RETURNING "space".contact_type.id INTO idx_contact_type;
		END IF;
	END IF;

	--- Get imaging pathology id
	IF imaging_pathology IS NOT NULL THEN
		SELECT INTO idx_imaging_pathology "space".imaging_pathology.id FROM "space".imaging_pathology WHERE "space".imaging_pathology.pathology_name = $7;
		IF idx_imaging_pathology IS NULL THEN
			INSERT INTO "space".imaging_pathology(pathology_name) VALUES ($7) RETURNING "space".imaging_pathology.id INTO idx_imaging_pathology;
		END IF;
	END IF;

	INSERT INTO "space".contact(
		contact_number,
		id_electrode,
		impedance,
		id_contact_size,
		id_contact_type,
		id_imaging_pathology
		) 
	VALUES (
		$1,
		$2,
		$3,
		idx_contact_size,
		idx_contact_type,
		idx_imaging_pathology)
	ON CONFLICT ON CONSTRAINT contact_un DO UPDATE SET
		impedance = $3,
		id_contact_size = idx_contact_size,
		id_contact_type = idx_contact_type,
		id_imaging_pathology = idx_imaging_pathology
	RETURNING
		"space".contact.id INTO $8;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;