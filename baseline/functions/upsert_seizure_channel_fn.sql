CREATE OR REPLACE FUNCTION signal.upsert_seizure_channel(
	IN dt_start TIMESTAMP,
	IN dt_stop TIMESTAMP,
	IN annotation_text character varying,
	IN ida_channel bigint[],
	IN seizure_type character varying,
	IN seizure_channel_type character varying,
	OUT success integer)
RETURNS integer AS
$BODY$

-- seizure is special kind of annotation 

DECLARE 

	--- Catalogs
	idx_seizure_type			INTEGER default 1;
	idx_seizure_channel_type	INTEGER default 1;

	--- Temporary values
	idx_channel			INTEGER;
	idx_annotation		INTEGER;
		
BEGIN	

	--- Get seizure type id
	IF seizure_type IS NOT NULL THEN
		SELECT INTO idx_seizure_type signal.seizure_type.id FROM signal.seizure_type WHERE signal.seizure_type.type_name = $5;
		IF idx_seizure_type IS NULL THEN
			INSERT INTO signal.seizure_type (type_name) VALUES ($5) RETURNING signal.seizure_type.id INTO idx_seizure_type;
		END IF;
	END IF;

	IF seizure_channel_type IS NOT NULL THEN
		SELECT INTO idx_seizure_channel_type signal.seizure_channel_type.id FROM signal.seizure_channel_type WHERE signal.seizure_channel_type.type_name = $6;
		IF idx_seizure_channel_type IS NULL THEN
			INSERT INTO signal.seizure_channel_type (type_name) VALUES ($6) RETURNING signal.seizure_channel_type.id INTO idx_seizure_channel_type;
		END IF;
	END IF;

	FOREACH idx_channel IN ARRAY ida_channel
	LOOP
		INSERT INTO signal.annotation_channel (event_start_utc , event_stop_utc, annotation_text, id_channel )
				VALUES ($1, $2, $3, idx_channel) ON CONFLICT ON CONSTRAINT annotation_channel_un DO UPDATE SET
				annotation_text = $3
				RETURNING signal.annotation_channel.id INTO idx_annotation;
		INSERT INTO signal.seizure (id_annotation_channel , id_seizure_channel_type , id_seizure_type ) 
				VALUES (idx_annotation, idx_seizure_channel_type, idx_seizure_type) ON CONFLICT ON CONSTRAINT seizure_un DO UPDATE SET
				id_seizure_type = idx_seizure_type;
	
	END LOOP;
	
	$7 = 1;
	
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
