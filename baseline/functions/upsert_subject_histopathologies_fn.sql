CREATE OR REPLACE FUNCTION demographic.upsert_subject_histopathologies(
	id_mined integer,
	in_dt_date_utc timestamp,
	histopathology_name character varying,
	OUT out_id_histopathology integer)
 RETURNS integer AS
$BODY$

DECLARE 
	idx_histopathology		INTEGER DEFAULT 1;

BEGIN	

	--- Get histopathology type id
	IF histopathology_name IS NOT NULL THEN
		SELECT INTO idx_histopathology demographic.histopathology.id FROM demographic.histopathology WHERE demographic.histopathology.histopathology_name = $3;
		IF idx_histopathology IS NULL THEN
			INSERT INTO demographic.histopathology(histopathology_name) VALUES ($3) RETURNING id INTO idx_histopathology;
		END IF;
	END IF;

	INSERT INTO demographic.subject_histopathologies(
		dt_date_utc,
		id_histopathology,
		id_mined_subject) 
	VALUES (
		$2,
		idx_histopathology,
		$1)
	ON CONFLICT ON CONSTRAINT subject_histopathologies_un DO NOTHING
	RETURNING
		demographic.subject_histopathologies.id INTO $4;

	IF $4 IS NULL THEN
		SELECT demographic.subject_histopathologies.id INTO $4 FROM demographic.subject_histopathologies WHERE
			dt_date_utc = $2
			AND id_histopathology = idx_histopathology
			AND id_mined_subject = $1;
	END IF;
	RETURN;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

