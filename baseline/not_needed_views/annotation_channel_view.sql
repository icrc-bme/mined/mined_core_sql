CREATE OR REPLACE VIEW signal.annotation_channel_view
AS SELECT ac.id,
    c.id_session,
    ac.event_start_utc,
    ac.event_stop_utc,
    ac.annotation_text,
    ac.id_channel,
    c.channel_name,
    tz.zone_name
   FROM signal.annotation_channel ac
     JOIN signal.channel c ON ac.id_channel = c.id
     INNER JOIN recording."session" s ON c.id_session = s.id
     INNER JOIN demographic.subject s2 ON s.id_mined_subject = s2.id_mined
     INNER JOIN demographic.institution i ON s2.id_institution = i.id
     INNER JOIN demographic.time_zone tz ON i.id_time_zone = tz.id;


