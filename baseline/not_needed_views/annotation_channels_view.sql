CREATE OR REPLACE VIEW signal.annotation_channels_view
AS SELECT ac.id AS id_annotation_channel,
    c.channel_name,
    c.id AS id_channel
   FROM
     signal.annotation_channel ac
     INNER JOIN signal.channel c ON ac.id_channel = c.id;
