CREATE OR REPLACE VIEW "space".intervention_contact_view
AS SELECT csi.id_subject_intervention,
    c.id AS id_contact,
    c.contact_number,
    e.electrode_name,
  c2.c_x,
    c2.c_y,
    c2.c_z,
    cs.system_name,
    cs.system_unit
   FROM space.contact_subjects_interventions csi
     JOIN space.contact c ON csi.id_contact = c.id
     JOIN space.coordinates c2 ON c.id = c2.id_contact
     JOIN space.coordinate_system cs ON c2.id_coordinate_system = cs.id
     JOIN space.electrode e ON e.id = c.id_electrode; 