DROP VIEW IF EXISTS signal.seizure_view;   
CREATE OR REPLACE VIEW signal.seizure_view
AS SELECT s.id,
    c.id_session,
    s.id_annotation_channel,
    ac.event_start_utc,
    ac.event_stop_utc,
    ac.annotation_text,
    c.id AS id_channel,
    c.channel_name,
    st.type_name AS seizure_type,
    sct.type_name AS seizure_channel_type,
    tz.zone_name
   FROM signal.seizure s
     JOIN signal.seizure_channel_type sct ON s.id_seizure_channel_type = sct.id
     JOIN signal.seizure_type st ON s.id_seizure_type = st.id
     JOIN signal.annotation_channel ac ON s.id_annotation_channel = ac.id
     JOIN signal.channel c ON ac.id_channel = c.id
     JOIN recording.session s2 ON c.id_session = s2.id
     JOIN demographic.subject s3 ON s2.id_mined_subject = s3.id_mined
     JOIN demographic.institution i ON s3.id_institution = i.id
     JOIN demographic.time_zone tz ON i.id_time_zone = tz.id;
     
    
