DROP VIEW  IF EXISTS recording.session_view;         
CREATE OR REPLACE VIEW recording.session_view
AS SELECT s.id,
    s.session_name,
    s.dt_start_utc,
    s.dt_stop_utc,
    s.acquisition_device,
    s.ip_address,
    m.modality_name,
    st.type_name,
    rr.reference_name,
    su.id_subject,
    su.id_mined,
    ( SELECT count(*) AS count
           FROM recording.trial
          WHERE trial.id_session = s.id) AS trial_count,
    tz.zone_name
   FROM recording.session s
     JOIN recording.session_type st ON s.id_session_type = st.id
     JOIN recording.modality m ON s.id_modality = m.id
     JOIN signal.recording_reference rr ON s.id_recording_reference = rr.id
     JOIN demographic.subject su ON s.id_mined_subject = su.id_mined
     JOIN demographic.institution i ON su.id_institution = i.id
     JOIN demographic.time_zone tz ON i.id_time_zone = tz.id;



