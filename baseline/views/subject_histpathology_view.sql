DROP VIEW IF EXISTS  demographic.subject_histopathology_view;    
CREATE OR REPLACE VIEW demographic.subject_histopathology_view
AS SELECT sh.id,
    sh.id_mined_subject,
    h.histopathology_name,
    sh.dt_date_utc,
    tz.zone_name
   FROM demographic.subject_histopathologies sh
     JOIN demographic.histopathology h ON sh.id_histopathology = h.id
     JOIN demographic.subject s ON sh.id_mined_subject = s.id_mined
     JOIN demographic.institution i ON s.id_institution = i.id
     JOIN demographic.time_zone tz ON i.id_time_zone = tz.id;

