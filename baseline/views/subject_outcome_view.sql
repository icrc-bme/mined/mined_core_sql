DROP VIEW  IF EXISTS demographic.subject_outcome_view;   
CREATE OR REPLACE VIEW demographic.subject_outcome_view
AS SELECT o.id_mined_subject,
    o.dt_date_utc,
    il.scale_name AS ilae,
    o.id,
    es.scale_name AS engel,
    tz.zone_name
   FROM demographic.subject_outcomes o
     JOIN demographic.ilae_scale il ON o.id_ilae_scale = il.id
     JOIN demographic.engel_scale es ON o.id_engel_scale = es.id
     JOIN demographic.subject s ON o.id_mined_subject = s.id_mined
     JOIN demographic.institution i ON s.id_institution = i.id
     JOIN demographic.time_zone tz ON i.id_time_zone = tz.id;   
