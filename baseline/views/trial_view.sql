DROP VIEW  IF EXISTS recording.trial_view;         
CREATE OR REPLACE VIEW recording.trial_view
AS SELECT t.id,
    s.id AS id_session,
    t.onset,
    t.duration,
    t.response_time,
    t.response,
    t.ttl_value,
    tt.type_name AS trial_type,
    t2.task_name,
    t2.task_description,
    t2.patient_instructions
   FROM recording.session s
     JOIN recording.trial t ON s.id = t.id_session
     JOIN recording.trial_type tt ON t.id_trial_type = tt.id
     JOIN recording.task t2 ON tt.id_task = t2.id;    
