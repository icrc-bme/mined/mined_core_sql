#! /bin/sh

# Process the optional arguments for psql
while getopts nU:h:p:d:P:v: option
  do
    case "${option}"
    in
    U) pg_user=${OPTARG};;
    h) pg_host=${OPTARG};;
    p) pg_port=${OPTARG};;
    d) pg_database=${OPTARG};;
    P) pg_password=${OPTARG};;
    v) version=${OPTARG};;
    n) dry_run=1;;
  esac
done

if [ ! -z "$pg_user" ]
then
  export PGUSER=${pg_user}
fi

if [ ! -z "$pg_host" ]
then
  export PGHOST=${pg_host}
fi

if [ ! -z "$pg_port" ]
then
  export PGPORT=${pg_port}
fi

if [ ! -z "$pg_password" ]
then
  export PGPASSWORD=${pg_password}
fi

if [ -z "$pg_database" ]
then
  pg_database='mined_core'
fi
export PGDATABASE=${pg_database}

if [ ! -z "$version" ]
then
  # Check if version is in correct format
  version_pattern="[[:digit:]][[:digit:]].[[:digit:]][[:digit:]].[[:digit:]][[:digit:]][[:digit:]][[:digit:]]"

  case $version in 
    $version_pattern)
      echo "Will update to version $version";;
    *)
      echo "Incorrect version pattern. Please use 00.00.0000 patern.";
      exit 0;;
  esac
else
  version='99.99.9999'
fi

if [ ! -z "$dry_run" ]
then
  dry_run=1
else
  dry_run=0
fi

# Query the database for the version number
db_version_string=$(psql -t -A -c "SELECT major_release_number, minor_release_number, point_release_number FROM util.schema_changes WHERE date_applied=(SELECT max(date_applied) FROM util.schema_changes)")
db_major=$(echo $db_version_string | cut -c 1-2)
db_minor=$(echo $db_version_string | cut -c 4-5)
db_point=$(echo $db_version_string | cut -c 7-10)
echo "Current database version: $db_major.$db_minor.$db_point"

# Get available versions (i.e. directories containing version)
update_dirs=$(ls ../updates)

# Find the next version number
exec_flag=0
for d in $update_dirs
do
  major=$(echo $d | cut -c 1-2)
  minor=$(echo $d | cut -c 4-5)
  point=$(echo $d | cut -c 7-10)

  current_version=$(echo $d | cut -c 1-10)

  # Execute update if the execution flag has been raised
  if [ $exec_flag = 1 ]
  then
  	echo "\n----- Updating MINED to version $major.$minor.$point -----\n"

    SQL_FILES=""

    VIEW_FILES=../updates/$d/views/*
    FUNCTION_FILES=../updates/$d/functions/*

    SQL_FILES="${SQL_FILES} ../updates/$d/$d.sql $VIEW_FILES $FUNCTION_FILES"
    
    whole_sql="$(cat $SQL_FILES)"

    version_query="INSERT INTO util.schema_changes (major_release_number, minor_release_number, point_release_number, script_name, date_applied) VALUES ('$major', '$minor','$point', 'update_db_mined_feature', NOW());"

    whole_sql="${whole_sql} ${version_query}"

    # Run everything in one transaction - when error occurs, everything is rolled back
    if [ $dry_run = 1 ]
    then
      echo "BEGIN; $whole_sql ROLLBACK;"  | psql -f -
    else
      echo "$whole_sql"  | psql -1 -f -
    fi
  fi

  if [ $current_version = $version ]
  then
    exit 1;
  fi

  # Check if we have found a version that corresponds to the DB
  if [ $db_major = $major ] && [ $db_minor = $minor ] && [ $db_point = $point ] 
  then
  	# Flip the execution flag
  	exec_flag=1
  fi

done
