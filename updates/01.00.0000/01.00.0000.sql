-------------- DROP ALL VIEWS ------------

DO $$
DECLARE drop_query VARCHAR(512);
BEGIN
FOR drop_query IN (
	SELECT 'DROP VIEW ' || table_schema || '.' || table_name || ';'
		FROM information_schema.views
	WHERE table_schema IN ('demographic', 'recording', 'space', 'signal', 'bivariate', 'mined_user')
	   	AND table_name !~ '^pg_'
	)
	LOOP
		EXECUTE drop_query;
	END LOOP;
END $$;


-------------- DROP ALL FUNCTIONS ------------

DO $$
DECLARE drop_query VARCHAR(512);
BEGIN
FOR drop_query IN (
	SELECT  'DROP FUNCTION ' || routines.specific_schema || '.' || routines.routine_name || ' CASCADE;'
	FROM information_schema.routines
	    LEFT JOIN information_schema.parameters ON routines.specific_name=parameters.specific_name
	WHERE routines.specific_schema IN ('demographic', 'recording', 'space', 'signal', 'bivariate', 'mined_user')
	GROUP BY routines.specific_schema, routines.routine_name
	)
	LOOP
		EXECUTE drop_query;
	END LOOP;
END $$;

-- Change the unique key in space.electrode
ALTER TABLE "space".electrode DROP CONSTRAINT electrode_un;
ALTER TABLE "space".electrode ADD CONSTRAINT electrode_un UNIQUE (electrode_name,id_mined_subject);

-- Add catalogs to demographic
CREATE TABLE demographic.ethnicity (
	id serial NOT NULL,
	ethnicity_name varchar(32) NOT NULL,
	CONSTRAINT ethnicity_pk PRIMARY KEY (id),
	CONSTRAINT ethnicity_un UNIQUE (ethnicity_name)
);

CREATE TABLE demographic.race (
	id serial NOT NULL,
	race_name varchar(32) NOT NULL,
	CONSTRAINT race_pk PRIMARY KEY (id),
	CONSTRAINT race_un UNIQUE (race_name)
);

CREATE TABLE demographic.mchugh_scale (
	id serial NOT NULL,
	scale_name varchar(32) NOT NULL,
	CONSTRAINT mchugh_scale_pk PRIMARY KEY (id),
	CONSTRAINT mchugh_scale_un UNIQUE (scale_name)
);
-- Insert basic entries to catalogs
INSERT INTO demographic.ethnicity (ethnicity_name) VALUES ('n/a'), ('unspecified');
INSERT INTO demographic.race (race_name) VALUES ('n/a'), ('unspecified');
INSERT INTO demographic.mchugh_scale (scale_name) VALUES ('n/a'), ('unspecified');

-- Add columns to demographic.subject
ALTER TABLE demographic.subject ADD id_ethnicity int4 NOT NULL DEFAULT 1;
ALTER TABLE demographic.subject ADD id_race int4 NOT NULL DEFAULT 1;

-- Create foreign keys in demographic.subject
ALTER TABLE demographic.subject ADD CONSTRAINT ethnicity_fk FOREIGN KEY (id_ethnicity) REFERENCES demographic.ethnicity(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE demographic.subject ADD CONSTRAINT race_fk FOREIGN KEY (id_race) REFERENCES demographic.race(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;

-- Adjust foreign keys in demographic.subject
ALTER TABLE demographic.subject DROP CONSTRAINT sex_fk;
ALTER TABLE demographic.subject ADD CONSTRAINT sex_fk FOREIGN KEY (id_sex) REFERENCES demographic.sex(id) MATCH FULL ON DELETE SET DEFAULT ON UPDATE CASCADE;
ALTER TABLE demographic.subject DROP CONSTRAINT institution_fk;
ALTER TABLE demographic.subject ADD CONSTRAINT institution_fk FOREIGN KEY (id_institution) REFERENCES demographic.institution(id) MATCH FULL ON DELETE SET DEFAULT ON UPDATE CASCADE;
ALTER TABLE demographic.subject DROP CONSTRAINT handedness_fk;
ALTER TABLE demographic.subject ADD CONSTRAINT handedness_fk FOREIGN KEY (id_handedness) REFERENCES demographic.handedness(id) MATCH FULL ON DELETE SET DEFAULT ON UPDATE CASCADE;
ALTER TABLE demographic.subject DROP CONSTRAINT subject_type_fk;
ALTER TABLE demographic.subject ADD CONSTRAINT subject_type_fk FOREIGN KEY (id_subject_type) REFERENCES demographic.subject_type(id) MATCH FULL ON DELETE SET DEFAULT ON UPDATE CASCADE;


-- Add column to demograpic.subject_outcomes
ALTER TABLE demographic.subject_outcomes ADD id_mchugh_scale int4 NOT NULL DEFAULT 1;

-- Create foreign keys in demograpic.subject_outcomes
ALTER TABLE demographic.subject_outcomes ADD CONSTRAINT machugh_scale_fk FOREIGN KEY (id_mchugh_scale) REFERENCES demographic.mchugh_scale(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;

-- Set defaults in demographic.subject_outcomes
ALTER TABLE demographic.subject_outcomes ALTER COLUMN id_ilae_scale SET DEFAULT 1;
ALTER TABLE demographic.subject_outcomes ALTER COLUMN id_engel_scale SET DEFAULT 1;

-- Adjust foreign keys in demographic.subject_outcomes
ALTER TABLE demographic.subject_outcomes DROP CONSTRAINT engel_scale_fk;
ALTER TABLE demographic.subject_outcomes ADD CONSTRAINT engel_scale_fk FOREIGN KEY (id_engel_scale) REFERENCES demographic.engel_scale(id) MATCH FULL ON DELETE SET DEFAULT ON UPDATE CASCADE;
ALTER TABLE demographic.subject_outcomes DROP CONSTRAINT ilae_scale_fk;
ALTER TABLE demographic.subject_outcomes ADD CONSTRAINT ilae_scale_fk FOREIGN KEY (id_ilae_scale) REFERENCES demographic.ilae_scale(id) MATCH FULL ON DELETE SET DEFAULT ON UPDATE CASCADE;


-- Rename column histopathology_name to histopathology_type
ALTER TABLE demographic.histopathology DROP CONSTRAINT histopathology_un;
ALTER TABLE demographic.histopathology RENAME COLUMN histopathology_name TO histopathology_type;

-- Create column histopathology_subtype
ALTER TABLE demographic.histopathology ADD histopathology_subtype varchar(1024);
UPDATE demographic.histopathology SET histopathology_subtype = 'n/a';
ALTER TABLE demographic.histopathology ALTER COLUMN histopathology_subtype SET NOT NULL;
ALTER TABLE demographic.histopathology ADD CONSTRAINT histopathology_un UNIQUE (histopathology_type,histopathology_subtype);

-- Insert catalog values
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('HS', 'I');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('HS', 'II');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('HS', 'III');

INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('FCD', 'Ia');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('FCD', 'Ib');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('FCD', 'Ic');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('FCD', 'IIa');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('FCD', 'IIb');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('FCD', 'IIIa');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('FCD', 'IIIb');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('FCD', 'IIIc');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('FCD', 'IIId');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('FCD', 'mMCD');

INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('Vascular Malformation', 'CCM');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('Vascular Malformation', 'AVM');

INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('Other Structural', 'Post-ischemic');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('Other Structural', 'Post-encephalitic');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('Other Structural', 'Post-traumatic');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('Other Structural', 'Post-surgical');

INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('LEAT', 'DNT');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('LEAT', 'GG');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('LEAT', 'PA');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('LEAT', 'PGNT');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('LEAT', 'PXA');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('LEAT', 'RGNT');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('LEAT', 'Other');

INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('Other MCD', 'Heterotopia');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('Other MCD', 'Polymicrogyria');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('Other MCD', 'Tuberous Sclerosis');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('Other MCD', 'Lissencephaly');
INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('Other MCD', 'Megalencephaly');

INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('Other', 'n/a');

INSERT INTO demographic.histopathology (histopathology_type, histopathology_subtype) VALUES ('Negative', 'n/a');

-- Create table recording.annotation_session_type
CREATE TABLE recording.annotation_session_type (
	id serial4 NOT NULL PRIMARY KEY,
	type_name varchar(1024) NOT NULL,
	subtype_name varchar(1024) NOT NULL
);

-- Insert catalog values
INSERT INTO recording.annotation_session_type (type_name, subtype_name) VALUES ('n/a', 'n/a');

INSERT INTO recording.annotation_session_type (type_name, subtype_name) VALUES ('sleep', 'n/a');
INSERT INTO recording.annotation_session_type (type_name, subtype_name) VALUES ('sleep', 'wake');
INSERT INTO recording.annotation_session_type (type_name, subtype_name) VALUES ('sleep', 'REM');
INSERT INTO recording.annotation_session_type (type_name, subtype_name) VALUES ('sleep', 'N1');
INSERT INTO recording.annotation_session_type (type_name, subtype_name) VALUES ('sleep', 'N2');
INSERT INTO recording.annotation_session_type (type_name, subtype_name) VALUES ('sleep', 'N3');

INSERT INTO recording.annotation_session_type (type_name, subtype_name) VALUES ('seizure', 'n/a');
INSERT INTO recording.annotation_session_type (type_name, subtype_name) VALUES ('seizure', 'unknown');
INSERT INTO recording.annotation_session_type (type_name, subtype_name) VALUES ('seizure', 'focal');
INSERT INTO recording.annotation_session_type (type_name, subtype_name) VALUES ('seizure', 'generalized');
INSERT INTO recording.annotation_session_type (type_name, subtype_name) VALUES ('seizure', 'propagated');
INSERT INTO recording.annotation_session_type (type_name, subtype_name) VALUES ('seizure', 'mixed');

-- Create table recording.annotation_channel_type
CREATE TABLE signal.annotation_channel_type (
	id serial4 NOT NULL PRIMARY KEY,
	type_name varchar(1024) NOT NULL,
	subtype_name varchar(1024) NOT NULL
); 

-- Insert catalog values
INSERT INTO signal.annotation_channel_type (type_name, subtype_name) VALUES ('n/a', 'n/a');

INSERT INTO signal.annotation_channel_type (type_name, subtype_name) VALUES ('seizure', 'seizure onset zone');
INSERT INTO signal.annotation_channel_type (type_name, subtype_name) VALUES ('seizure', 'irritative zone');
INSERT INTO signal.annotation_channel_type (type_name, subtype_name) VALUES ('seizure', 'early propagation');
INSERT INTO signal.annotation_channel_type (type_name, subtype_name) VALUES ('seizure', 'normal');

INSERT INTO signal.annotation_channel_type (type_name, subtype_name) VALUES ('artifact', 'muscle');
INSERT INTO signal.annotation_channel_type (type_name, subtype_name) VALUES ('artifact', 'noise');
INSERT INTO signal.annotation_channel_type (type_name, subtype_name) VALUES ('artifact', 'machine');

-- Alter table recording.annotation_session
ALTER TABLE recording.annotation_session ADD id_annotation_session_type int4 NOT NULL DEFAULT 1;
ALTER TABLE recording.annotation_session ADD CONSTRAINT annotation_session_type_fk FOREIGN KEY (id_annotation_session_type) REFERENCES recording.annotation_session_type(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE recording.annotation_session DROP CONSTRAINT annotation_session_un;
ALTER TABLE recording.annotation_session ADD CONSTRAINT annotation_session_un UNIQUE (event_start_utc,event_stop_utc,id_session,id_annotation_session_type);

-- Alter table signal.annotation_channel
ALTER TABLE signal.annotation_channel ADD id_annotation_channel_type int4 NOT NULL DEFAULT 1;
ALTER TABLE signal.annotation_channel ADD CONSTRAINT annotation_channel_type_fk FOREIGN KEY (id_annotation_channel_type) REFERENCES signal.annotation_channel_type(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE signal.annotation_channel DROP CONSTRAINT annotation_channel_un;
ALTER TABLE signal.annotation_channel ADD CONSTRAINT annotation_channel_un UNIQUE (event_start_utc,event_stop_utc,id_channel,id_annotation_channel_type);

-- Transfer seizure annotations to the new structure

-- First insert seizures to recording_annotation_session using earliest start time a latest end time
DO
$do$
DECLARE
	r	record;
	cr	record;
	annotation_session_type_id		int;
BEGIN 
   FOR r IN 
	    SELECT
			c.id_session,
			st.type_name AS old_type_name,
			MIN(ac.event_start_utc) AS min_start,
			MAX(ac.event_stop_utc) AS min_stop
		FROM
			signal.seizure s
    		JOIN signal.seizure_channel_type sct ON s.id_seizure_channel_type = sct.id
    		JOIN signal.seizure_type st ON s.id_seizure_type = st.id
    		JOIN signal.annotation_channel ac ON s.id_annotation_channel = ac.id
    		JOIN signal.channel c ON ac.id_channel = c.id
		GROUP BY 
			st.type_name,
			c.id_session
   LOOP

   		SELECT id INTO annotation_session_type_id
   		FROM 
   			recording.annotation_session_type
   		WHERE 
   			type_name = 'seizure'
   			AND subtype_name = r.old_type_name;

	    INSERT INTO recording.annotation_session
	        (event_start_utc, event_stop_utc, id_session, annotation_text, id_annotation_session_type)
	    VALUES
	      	(r.min_start, r.min_stop, r.id_session, '', annotation_session_type_id);
   END LOOP;
END
$do$;

-- No transfer individual channel seizure annotations to 
DO
$do$
DECLARE
	r	record;
	cr	record;
	annotation_channel_type_id		int;
BEGIN 
    FOR r IN 
	    SELECT
	    	ac.id,
			ac.id_channel,
			sct.type_name AS old_type_name,
			ac.event_start_utc,
			ac.event_stop_utc 
		FROM
			signal.seizure s
    		JOIN signal.seizure_channel_type sct ON s.id_seizure_channel_type = sct.id
    		JOIN signal.seizure_type st ON s.id_seizure_type = st.id
    		JOIN signal.annotation_channel ac ON s.id_annotation_channel = ac.id
    LOOP
   		SELECT id INTO annotation_channel_type_id
   		FROM 
   			signal.annotation_channel_type
   		WHERE 
   			type_name = 'seizure'
   			AND subtype_name = r.old_type_name;

   		UPDATE
   			signal.annotation_channel
   		SET 
   			id_annotation_channel_type = annotation_channel_type_id
   		WHERE
   			id = r.id;

   END LOOP;
END
$do$;

-- We have all seizure transformation done...get rid of old seizure tables
DROP TABLE signal.seizure;
DROP TABLE signal.seizure_channel_type;
DROP TABLE signal.seizure_type;

-- Remove mined_user tables and schema if it exists
DROP TABLE IF EXISTS mined_user.mined_user;
DROP TABLE IF EXISTS mined_user.access;
DROP SCHEMA IF EXISTS mined_user;
