CREATE OR REPLACE FUNCTION demographic.insert_institution(
    IN zone_name character varying,
    IN institution_name character varying,
    OUT id integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalog indices
	idx_time_zone		INTEGER default 573; --- UTC

BEGIN	

	--- Get time zone index
	SELECT INTO idx_time_zone demographic.time_zone.id FROM demographic.time_zone WHERE demographic.time_zone.zone_name = $1;
	IF idx_time_zone IS NULL THEN
		RAISE data_exception USING MESSAGE = 'No such time zone in time zone catalog: zone_name = ' || $1;
	END IF;

	INSERT INTO demographic.institution(
		id_time_zone,
		institution_name)
	VALUES (
		idx_time_zone,
		$2)
	RETURNING
		demographic.institution.id INTO $3;
	RETURN;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;