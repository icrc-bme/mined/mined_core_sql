CREATE OR REPLACE FUNCTION signal.save_channel(
	IN id_channel integer,
	IN channel_name character varying,
	IN id_session integer,
	IN sampling_frequency float,
	IN low_cutoff float,
	IN high_cutoff float,
	IN unit character varying,
	IN acquisition_number integer,
	IN quality_type character varying,
	IN channel_type character varying,
	IN recording_reference character varying,
	IN ida_contact BIGINT[],
    OUT out_id_channel integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalogs
	idx_quality_type			INTEGER default 1;
	idx_channel_type			INTEGER default 1;
	idx_recording_reference		INTEGER default 1;

	--- Temporary values
	idx_channel				INTEGER default 0;
	idx_contact				INTEGER;

BEGIN	

	--- Get quality type id
	IF quality_type IS NOT NULL THEN
		SELECT INTO idx_quality_type signal.quality_type.id FROM signal.quality_type WHERE signal.quality_type.type_name = $9;
		IF idx_quality_type IS NULL THEN
			$13 = -1201;
			RETURN;
		END IF;
	END IF;

	--- Get channel type id
	IF channel_type IS NOT NULL THEN
		SELECT INTO idx_channel_type signal.channel_type.id FROM signal.channel_type WHERE signal.channel_type.type_name = $10;
		IF idx_channel_type IS NULL THEN
			$13 = -1202;
			RETURN;
		END IF;
	END IF;


	-- Get recording reference id
	IF recording_reference IS NOT NULL THEN
		SELECT INTO idx_recording_reference signal.recording_reference.id FROM signal.recording_reference WHERE signal.recording_reference.reference_name = $11;
		IF idx_recording_reference IS NULL THEN
			$13 = -1203;
			RETURN;
		END IF;
	END IF;

	
	BEGIN
		IF $1 = 0 THEN
			INSERT INTO signal.channel(
				channel_name,
				sampling_frequency,
				id_quality_type,
				id_channel_type,
				id_session,
				low_cutoff,
				high_cutoff,
				unit,
				acquisition_number)
			VALUES (
				$2,
				$4,
				idx_quality_type,
				idx_channel_type,
				$3,
				$5,
				$6,
				$7,
				$8)
			RETURNING
				signal.channel.id INTO idx_channel;

			IF ida_contact IS NOT NULL THEN
				FOREACH idx_contact IN ARRAY ida_contact
				LOOP
					INSERT INTO signal.channel_contact (id_channel, id_contact, id_recording_reference) VALUES (idx_channel, idx_contact, idx_recording_reference); 
				END LOOP;
			END IF;
			$13 = idx_channel;

		ELSE

			DELETE FROM signal.channel_contact WHERE signal.channel_contact.id_channel = $1;

			UPDATE signal.channel SET 
				channel_name = $2,
				sampling_frequency = $4,
				id_quality_type = idx_quality_type,
				id_channel_type = idx_channel_type,
				id_session = $3,
				low_cutoff = $5,
				high_cutoff = $6,
				unit = $7,
				acquisition_number = $8
			WHERE signal.channel.id=$1;

			IF ida_contact IS NOT NULL THEN
				FOREACH idx_contact IN ARRAY ida_contact
				LOOP
					INSERT INTO signal.channel_contact (id_channel, id_contact, id_recording_reference) VALUES ($1, idx_contact, idx_recording_reference); 
				END LOOP;
			END IF;
			$13 = $1;
		END IF;
		EXCEPTION WHEN OTHERS THEN
			$13 = -255;
	END;


END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;