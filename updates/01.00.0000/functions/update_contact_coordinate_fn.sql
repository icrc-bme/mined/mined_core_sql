DROP FUNCTION IF EXISTS space.update_contact_coordinate;
CREATE OR REPLACE FUNCTION space.update_contact_coordinate(id_contact bigint, cx real, cy real, cz real, system_name character varying, system_unit character varying, OUT out_id_contact bigint)
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
--- add 
--- "space".update_contact_coordinate if exists
--- "space".update_contact_anatomy
--- in one step
DECLARE 

	idx_coordinate_system 	int4 DEFAULT 0;
	idx_coordinate 			int4 DEFAULT 0;

BEGIN	
		-- update coordinate
	SELECT INTO idx_coordinate_system "space".coordinate_system.id FROM "space".coordinate_system WHERE "space".coordinate_system.system_name = $5 AND "space".coordinate_system.system_unit = $6;
	IF idx_coordinate_system = 0 OR idx_coordinate_system IS NULL THEN
		$7 = -3;
		RETURN;
	END IF;
	SELECT INTO idx_coordinate "space".coordinates.id FROM "space".coordinates WHERE "space".coordinates.id_contact = $1;


	UPDATE "space".coordinates SET c_x = $2,
								c_y = $3,
								c_z = $4,
								id_coordinate_system = idx_coordinate_system
							WHERE  "space".coordinates.id = idx_coordinate;
	$7 = $1;

--	EXCEPTION WHEN OTHERS THEN
--		$7 = -254;
--	RETURN;
END
$function$
;