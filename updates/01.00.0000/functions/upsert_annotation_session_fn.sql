CREATE OR REPLACE FUNCTION recording.upsert_annotation_session(
    IN event_start_utc TIMESTAMP,
    IN id_session integer,
    IN event_stop_utc TIMESTAMP,
    IN annotation_text character varying,
    OUT id_annotation integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalog indices

BEGIN

	INSERT INTO recording.annotation_session(
		event_start_utc,
		event_stop_utc,
		id_session,
		annotation_text)
	VALUES (
		$1,
		$3,
		$2,
		$4)
	ON CONFLICT ON CONSTRAINT annotation_session_un DO UPDATE SET
		annotation_text = $4
	RETURNING
		recording.annotation_session.id INTO $5;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 
