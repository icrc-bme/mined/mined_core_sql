DROP VIEW  IF EXISTS recording.annotation_session_view;         
CREATE OR REPLACE VIEW recording.annotation_session_view
AS SELECT ases.id,
    ases.id_session,
    ases.event_start_utc,
    ases.event_stop_utc,
    ases.annotation_text,
    ast.type_name,
    ast.subtype_name,
    tz.zone_name
   FROM recording.annotation_session AS ases
     JOIN recording.annotation_session_type ast ON ases.id_annotation_session_type = ast.id
     JOIN recording.session s ON ases.id_session = s.id
     JOIN demographic.subject s2 ON s.id_mined_subject = s2.id_mined
     JOIN demographic.institution i ON s2.id_institution = i.id
     JOIN demographic.time_zone tz ON i.id_time_zone = tz.id; 