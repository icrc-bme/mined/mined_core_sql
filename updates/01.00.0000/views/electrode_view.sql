CREATE OR REPLACE VIEW "space".electrode_view
AS SELECT e.id,
  e.id_mined_subject,
    e.electrode_name,
    et.id AS id_electrode_type,
    et.manufacturer,
    et.contact_material,
    et.type_name AS electrode_type,
    et.macro_contact_distance,
    et.micro_contact_distance,
    et.dimension_a,
    et.dimension_b
   FROM space.electrode e
     JOIN space.electrode_type et ON e.id_electrode_type = et.id;
