-------------- DROP ALL VIEWS ------------

DO $$
DECLARE drop_query VARCHAR(512);
BEGIN
FOR drop_query IN (
	SELECT 'DROP VIEW ' || table_schema || '.' || table_name || ';'
		FROM information_schema.views
	WHERE table_schema IN ('demographic', 'recording', 'space', 'signal', 'bivariate', 'mined_user')
	   	AND table_name !~ '^pg_'
	)
	LOOP
		EXECUTE drop_query;
	END LOOP;
END $$;


-------------- DROP ALL FUNCTIONS ------------

DO $$
DECLARE drop_query VARCHAR(512);
BEGIN
FOR drop_query IN (
	SELECT  'DROP FUNCTION ' || routines.specific_schema || '.' || routines.routine_name || ' CASCADE;'
	FROM information_schema.routines
	    LEFT JOIN information_schema.parameters ON routines.specific_name=parameters.specific_name
	WHERE routines.specific_schema IN ('demographic', 'recording', 'space', 'signal', 'bivariate', 'mined_user')
	GROUP BY routines.specific_schema, routines.routine_name
	)
	LOOP
		EXECUTE drop_query;
	END LOOP;
END $$;

-- Link trial table to annotation session
ALTER TABLE recording.trial ADD COLUMN id_annotation_session int4;
ALTER TABLE recording.trial ADD CONSTRAINT annotation_session_fk FOREIGN KEY (id_annotation_session) REFERENCES recording.annotation_session(id) ON DELETE SET NULL ON UPDATE CASCADE;

-- Remove link to session from trial
ALTER TABLE recording.trial DROP COLUMN id_session;

-- Drop columns onset and duration from trial
ALTER TABLE recording.trial DROP COLUMN onset;
ALTER TABLE recording.trial DROP COLUMN duration;

