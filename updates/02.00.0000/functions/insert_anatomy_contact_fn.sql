CREATE OR REPLACE FUNCTION "space".insert_anatomy_contact(
	IN id_anatomy integer,
	IN id_contact integer,
	IN manual_assign bool,
    OUT id integer)
  RETURNS integer AS
$BODY$

DECLARE 

BEGIN

	INSERT INTO "space".anatomy_contact(
		id_anatomy,
		id_contact,
		manual_assign) 
	VALUES (
		$1,
		$2,
		$3)
	RETURNING
		"space".anatomy_contact.id INTO $3;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;