CREATE OR REPLACE FUNCTION signal.insert_annotation_channel(
    IN event_start_utc TIMESTAMP,
    IN id_channel integer,
    IN event_stop_utc TIMESTAMP,
    IN annotation_text character varying,
    OUT id_annotation integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalog indices

BEGIN

	INSERT INTO signal.annotation_channel(
		event_start_utc,
		event_stop_utc,
		id_channel,
		annotation_text)
	VALUES (
		$1,
		$3,
		$2,
		$4)
	RETURNING
		signal.annotation_channel.id INTO $5;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 
