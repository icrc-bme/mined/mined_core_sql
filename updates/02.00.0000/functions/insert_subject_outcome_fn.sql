CREATE OR REPLACE FUNCTION demographic.insert_outcome(
    IN outcome_date TIMESTAMP,
    IN id_mined integer,
    IN engel_scale_name character varying,
    IN ilae_scale_name character varying,
    IN mchugh_scale_name character varying,
    OUT id_outcome integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalog indices
	idx_engel_scale		INTEGER default 1;
	idx_ilae_scale		INTEGER default 1;
	idx_mchugh_scale	INTEGER default 1;

BEGIN

	--- Get engel scale id
	IF engel_scale_name IS NOT NULL THEN
		SELECT INTO idx_engel_scale demographic.engel_scale.id FROM demographic.engel_scale WHERE demographic.engel_scale.scale_name = $3;
		IF idx_engel_scale IS NULL THEN
			INSERT INTO demographic.engel_scale(scale_name) VALUES ($3) RETURNING id INTO idx_engel_scale;
		END IF;
	END IF;

	--- Get ilae scale id
	IF ilae_scale_name IS NOT NULL THEN
		SELECT INTO idx_ilae_scale demographic.ilae_scale.id FROM demographic.ilae_scale WHERE demographic.ilae_scale.scale_name = $4;
		IF idx_ilae_scale IS NULL THEN
			INSERT INTO demographic.ilae_scale(scale_name) VALUES ($4) RETURNING id INTO idx_ilae_scale;
		END IF;
	END IF;

	--- Get mchugh scale id
	IF mchugh_scale_name IS NOT NULL THEN
		SELECT INTO idx_mchugh_scale demographic.mchugh_scale.id FROM demographic.mchugh_scale WHERE demographic.mchugh_scale.mchugh_name = $5;
		IF idx_mchugh_scale IS NULL THEN
			INSERT INTO demographic.mchugh_scale(scale_name) VALUES ($5) RETURNING id INTO idx_mchugh_scale;
		END IF;
	END IF;

	INSERT INTO demographic.subject_outcomes(
		dt_date_utc,
		id_mined_subject,
		id_engel_scale,
		id_ilae_scale,
		idx_mchugh_scale)
	VALUES (
		$1,
		$2,
		idx_engel_scale,
		idx_ilae_scale,
		idx_mchugh_scale)
	RETURNING
		demographic.outcome.id INTO $6;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 
