CREATE OR REPLACE FUNCTION demographic.save_subject_outcome(
	id_outcome INTEGER,
	dt_date_utc TIMESTAMP,
	id_mined INTEGER,
	scale_name_engel character varying,
	scale_name_ilae character varying,
	scale_name_mchugh character varying,
	OUT out_id_outcom INTEGER)
RETURNS INTEGER AS
$BODY$

DECLARE 

	--- Catalog indices
	idx_engel				INTEGER DEFAULT 1;
	idx_ilae				INTEGER DEFAULT 1;
	idx_mchugh				INTEGER DEFAULT 1;

BEGIN	

	--- Get engel scale id
	SELECT INTO idx_engel demographic.engel_scale.id FROM demographic.engel_scale WHERE demographic.engel_scale.scale_name = $4;
	IF idx_engel = 0 OR idx_engel IS NULL THEN
		$7 = -401;
		RETURN;
	END IF;

	--- Get ilae scale id
	SELECT INTO idx_ilae demographic.ilae_scale.id FROM demographic.ilae_scale WHERE demographic.ilae_scale.scale_name = $5;
	IF idx_ilae = 0 OR idx_ilae IS NULL THEN
		$7 = -402;
		RETURN;
	END IF;

	--- Get mchugh scale id
	SELECT INTO idx_mchugh demographic.mchugh_scale.id FROM demographic.mchugh_scale WHERE demographic.mchugh_scale.scale_name = $6;
	IF idx_mchugh = 0 OR idx_mchugh IS NULL THEN
		$7 = -403;
		RETURN;
	END IF;

	BEGIN
		IF $1 = 0 THEN
			INSERT INTO demographic.subject_outcomes( dt_date_utc, id_mined_subject, id_ilae_scale, id_engel_scale, id_mchugh_scale) VALUES( $2, $3, idx_ilae, idx_engel, idx_mchugh) RETURNING demographic.subject_outcomes.id INTO $7;
		ELSE
			UPDATE demographic.subject_outcomes SET dt_date_utc = $2, id_ilae_scale = idx_ilae, id_engel_scale = idx_engel, id_mchugh_scale = idx_mchugh WHERE demographic.subject_outcomes.id = $1;
			$7 = $1;
		END IF;
	EXCEPTION WHEN OTHERS THEN
		$7 = -255;
	END;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
