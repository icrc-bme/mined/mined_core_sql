CREATE OR REPLACE FUNCTION demographic.save_subject(
	IN id_mined_subject integer,
    IN id_institution integer,
    IN id_subject integer,
    IN subject_type character varying,
    IN sex character varying,
    IN handedness character varying,
    IN birth_date TIMESTAMP,
    IN seizure_onset_age real,
    IN note character varying,
    OUT id_mined integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalog indices
	idx_type		INTEGER default 1;
	idx_sex			INTEGER default 1;
	idx_handedness  INTEGER default 1;

BEGIN

	--- Get subject type id
	IF $4 IS NOT NULL THEN
		SELECT INTO idx_type demographic.subject_type.id FROM demographic.subject_type WHERE demographic.subject_type.type_name = $4;
		IF idx_type IS NULL THEN
			$10 = -201;
			RETURN;
		END IF;
	END IF;

	--- Get sex id
	IF $5 IS NOT NULL THEN
		SELECT INTO idx_sex demographic.sex.id FROM demographic.sex WHERE demographic.sex.sex_name = $5;
		IF idx_sex IS NULL THEN
			$10 = -202;
			RETURN;
		END IF;
	END IF;

	--- Get handedness id
	IF $6 IS NOT NULL THEN
		SELECT INTO idx_handedness demographic.handedness.id FROM demographic.handedness WHERE demographic.handedness.handedness_name = $6;
		IF idx_handedness IS NULL THEN
			$10 = -203;
			RETURN;
		END IF;
	END IF;

	BEGIN
	IF $1 = 0 THEN
		INSERT INTO demographic.subject(
			id_institution,
			id_subject,
			id_subject_type,
			id_sex,
			id_handedness,
			dt_birth,
			seizure_onset_age,
			note)
		VALUES (
			$2,
			$3,
			idx_type,
			idx_sex,
			idx_handedness,
			birth_date,
			$8,
			$9)
		RETURNING
			demographic.subject.id_mined INTO $10;
	ELSE
		UPDATE demographic.subject SET 
			id_institution = $2,
			id_subject = $3,
			id_subject_type = idx_type,
			id_sex = idx_sex,
			id_handedness = idx_handedness,
			dt_birth = birth_date,
			seizure_onset_age = $8,
			note = $9
		WHERE  demographic.subject.id_mined = $1;
		$10 = $1;
	END IF;
	EXCEPTION WHEN OTHERS THEN
		$10 = -255;
	END;
	
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 
