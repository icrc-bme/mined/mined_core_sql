DROP FUNCTION IF EXISTS space.update_contact;
CREATE OR REPLACE FUNCTION space.update_contact(id_contact bigint, contact_number int4, impedance double precision, contact_size_name character varying, contact_size_value real, contact_type character varying, id_electrode bigint, imaging_pathology character VARYING, OUT out_id_contact bigint)
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
--- add 
--- "space".update_contact_coordinate if exists
--- "space".update_contact_anatomy
--- in one step
DECLARE 
	idx_contact_size 		int4 DEFAULT 0;
	idx_contact_type 		int4 DEFAULT 0;
	idx_imaging_pathology	int4 DEFAULT 0;

BEGIN	
	
	IF $6 IS NULL THEN
		SELECT INTO idx_contact_size "space".contact_size.id FROM "space".contact_size WHERE "space".contact_size.size_name = $4;
	ELSE
		SELECT INTO idx_contact_size "space".contact_size.id FROM "space".contact_size WHERE "space".contact_size.size_name = $4 AND "space".contact_size.size_value = $5;
	END IF;
	IF idx_contact_size = 0 OR idx_contact_size IS NULL THEN
		$8 = -1;
		RETURN;
	END IF;

	SELECT INTO idx_contact_type "space".contact_type.id FROM "space".contact_type WHERE "space".contact_type.type_name = $6;
	IF idx_contact_type = 0 OR idx_contact_type IS NULL THEN
		$8 = -2;
		RETURN;
	END IF;
	
	IF $7 IS NOT NULL THEN
		SELECT INTO idx_imaging_pathology "space".imaging_pathology.id FROM "space".imaging_pathology WHERE "space".imaging_pathology.pathology_name = $8;
		IF idx_imaging_pathology IS NULL THEN
			$8 = -4;
			RETURN;
		END IF;
	END IF;


	BEGIN
		-- update contact
		--id_contact is not changed 
		UPDATE "space".contact SET contact_number = $2,
									impedance = $3,
									id_contact_size = idx_contact_size,
									id_contact_type = idx_contact_type,
									id_electrode = $7,
									id_imaging_pathology = idx_imaging_pathology
							WHERE  "space".contact.id = $1;
		$9 = $1;
	EXCEPTION WHEN OTHERS THEN
		$9 = -254;
	END;
RETURN;
END
$function$
;



