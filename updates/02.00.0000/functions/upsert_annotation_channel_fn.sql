CREATE OR REPLACE FUNCTION signal.upsert_annotation_channel(
    IN dt_start TIMESTAMP,
	IN dt_stop TIMESTAMP,								
	IN annotation_text character varying,
	IN ida_channel bigint[],
    OUT id_annotation integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Temporary values
	idx_channel			INTEGER;

BEGIN

	FOREACH idx_channel IN ARRAY ida_channel
		LOOP
			INSERT INTO signal.annotation_channel(
				event_start_utc,
				event_stop_utc,
				annotation_text,
				id_channel)
			VALUES (
				$1,
				$2,
				$3,
				idx_channel)
			ON CONFLICT ON CONSTRAINT annotation_channel_un DO UPDATE SET
				annotation_text = $3;
		END LOOP;

	$5 = 1;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 
