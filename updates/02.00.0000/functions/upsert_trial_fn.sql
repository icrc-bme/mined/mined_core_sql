CREATE OR REPLACE FUNCTION recording.upsert_trial(
    IN onset integer,
    IN id_session integer,
    IN duration integer,
    IN trial_name character varying,
    IN task_name character varying,
    IN response_time integer,
    IN response character varying,
    IN ttl_value integer,
    OUT id_trial integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalog indices
	idx_task		INTEGER default 1;
	idx_type		INTEGER default 1;

BEGIN

	--- Get task id
	IF task_name IS NOT NULL THEN
		SELECT INTO idx_task recording.task.id FROM recording.task WHERE recording.task.task_name = $5;
		IF idx_task IS NULL THEN
			INSERT INTO recording.task(task_name) VALUES ($5) RETURNING id INTO idx_task;
		END IF;
	END IF;

	--- Get trial type id
	IF trial_name IS NOT NULL THEN
		SELECT INTO idx_type recording.trial_type.id FROM recording.trial_type WHERE recording.trial_type.type_name = $4 AND recording.trial_type.id_task = idx_task;
		IF idx_type IS NULL THEN
			INSERT INTO recording.trial_type(type_name, id_task) VALUES ($4, idx_task) RETURNING id INTO idx_type;
		END IF;
	END IF;

	INSERT INTO recording.trial(
		onset,
		duration,
		response_time,
		response,
		ttl_value,
		id_trial_type,
		id_session)
	VALUES (
		$1,
		$3,
		$6,
		$7,
		$8,
		idx_type,
		$2)
	ON CONFLICT ON CONSTRAINT trial_un DO UPDATE SET
		duration = $3,
		response_time = $6,
		response = $7,
		ttl_value = $8
	RETURNING
		recording.trial.id INTO $9;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 
