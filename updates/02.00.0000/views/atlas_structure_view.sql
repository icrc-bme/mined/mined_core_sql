DROP VIEW IF EXISTS "space".atlas_structure_view;    
CREATE OR REPLACE VIEW "space".atlas_structure_view
AS SELECT a.id_atlas_type,
    aa.id AS id_anatomy_structure,
    aa.structure_name
   FROM space.anatomy a
     JOIN space.anatomy_structure aa ON a.id_anatomy_structure = aa.id;
