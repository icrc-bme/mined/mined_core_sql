DROP VIEW IF EXISTS "space".contact_anatomy_view;       
CREATE OR REPLACE VIEW "space".contact_anatomy_view
AS SELECT ac.id_contact,
    a.id AS id_anatomy,
    a.hemisphere,
    a.brain_area,
    ac.manual_assign,
    at.type_name AS atlas_name,
    st.structure_name,
    a.id_atlas_type,
    a.id_anatomy_structure,
    a.id_lobe,
    l.lobe_name
   FROM space.anatomy_contact ac
     JOIN space.anatomy a ON ac.id_anatomy = a.id
     JOIN space.atlas_type at ON a.id_atlas_type = at.id
     JOIN space.anatomy_structure st ON a.id_anatomy_structure = st.id
     JOIN space.lobe l ON a.id_lobe = l.id;
    

