-------------- DROP ALL VIEWS ------------

DO $$
DECLARE drop_query VARCHAR(512);
BEGIN
FOR drop_query IN (
	SELECT 'DROP VIEW ' || table_schema || '.' || table_name || ';'
		FROM information_schema.views
	WHERE table_schema IN ('demographic', 'recording', 'space', 'signal', 'bivariate', 'mined_user')
	   	AND table_name !~ '^pg_'
	)
	LOOP
		EXECUTE drop_query;
	END LOOP;
END $$;


-------------- DROP ALL FUNCTIONS ------------

DO $$
DECLARE drop_query VARCHAR(512);
BEGIN
FOR drop_query IN (
	SELECT  'DROP FUNCTION ' || routines.specific_schema || '.' || routines.routine_name || ' CASCADE;'
	FROM information_schema.routines
	    LEFT JOIN information_schema.parameters ON routines.specific_name=parameters.specific_name
	WHERE routines.specific_schema IN ('demographic', 'recording', 'space', 'signal', 'bivariate', 'mined_user')
	GROUP BY routines.specific_schema, routines.routine_name
	)
	LOOP
		EXECUTE drop_query;
	END LOOP;
END $$;

-- Add column quolie89
ALTER TABLE demographic.subject_outcomes ADD  quolie89 NUMERIC(5,2) NOT NULL DEFAULT -1;
COMMENT ON COLUMN demographic.subject_outcomes.quolie89 IS '-1 stands for not entered';

-- Add column quolie10
ALTER TABLE demographic.subject_outcomes ADD quolie10 NUMERIC(5,2) NOT NULL DEFAULT -1;
COMMENT ON COLUMN demographic.subject_outcomes.quolie10 IS '-1 stands for not entered';

-- Create guest user

-- Create guest user and add SELECT privileges
DO $$
BEGIN
    IF NOT EXISTS (
        SELECT usename
        FROM pg_user
        WHERE usename = 'guest'
    ) THEN
        CREATE ROLE guest LOGIN PASSWORD 'mined_guest';
        GRANT CONNECT ON DATABASE mined_core TO guest;

        GRANT USAGE ON SCHEMA demographic TO guest;
        GRANT USAGE ON SCHEMA signal TO guest;
        GRANT USAGE ON SCHEMA space TO guest;
        GRANT USAGE ON SCHEMA recording TO guest;

        GRANT SELECT ON ALL TABLES IN SCHEMA demographic TO guest;
        GRANT SELECT ON ALL TABLES IN SCHEMA signal TO guest;
        GRANT SELECT ON ALL TABLES IN SCHEMA space TO guest;
        GRANT SELECT ON ALL TABLES IN SCHEMA recording TO guest;

        ALTER DEFAULT PRIVILEGES IN SCHEMA demographic GRANT SELECT ON TABLES TO guest;
        ALTER DEFAULT PRIVILEGES IN SCHEMA signal GRANT SELECT ON TABLES TO guest;
        ALTER DEFAULT PRIVILEGES IN SCHEMA space GRANT SELECT ON TABLES TO guest;
        ALTER DEFAULT PRIVILEGES IN SCHEMA recording GRANT SELECT ON TABLES TO guest;
    END IF;
END $$;
