CREATE OR REPLACE FUNCTION signal.insert_channel(
	IN channel_name character varying,
	IN id_session integer,
	IN sampling_frequency float,
	IN low_cutoff float,
	IN high_cutoff float,
	IN unit character varying,
	IN acquisition_number integer,
	IN quality_type character varying,
	IN channel_type character varying,
    OUT id integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalogs
	idx_quality_type			INTEGER default 1;
	idx_channel_type			INTEGER default 1;

BEGIN	

	--- Get quality type id
	IF quality_type IS NOT NULL THEN
		SELECT INTO idx_quality_type signal.quality_type.id FROM signal.quality_type WHERE signal.quality_type.type_name = $8;
		IF idx_quality_type IS NULL THEN
			INSERT INTO signal.quality_type(type_name) VALUES ($8) RETURNING signal.quality_type.id INTO idx_quality_type;
		END IF;
	END IF;

	--- Get channel type id
	IF channel_type IS NOT NULL THEN
		SELECT INTO idx_channel_type signal.channel_type.id FROM signal.channel_type WHERE signal.channel_type.type_name = $9;
		IF idx_channel_type IS NULL THEN
			INSERT INTO signal.channel_type(type_name) VALUES ($9) RETURNING signal.channel_type.id INTO idx_channel_type;
		END IF;
	END IF;
	
	INSERT INTO signal.channel(
		channel_name,
		sampling_frequency,
		id_quality_type,
		id_channel_type,
		id_session,
		low_cutoff,
		high_cutoff,
		unit,
		acquisition_number)
	VALUES (
		$1,
		$3,
		idx_quality_type,
		idx_channel_type,
		$2,
		$4,
		$5,
		$6,
		$7)
	RETURNING
		signal.channel.id INTO $10;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;