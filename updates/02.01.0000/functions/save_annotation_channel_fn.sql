CREATE OR REPLACE FUNCTION signal.save_annotation_channel(
	IN insert_annotation_channel boolean, 
	IN dt_start timestamp without time zone,
	IN dt_stop timestamp without time zone,								
	IN annotation_text character varying,
	IN annotation_type character varying,
	IN annotation_subtype character varying,
	IN ida_channel bigint[],
	OUT success integer)
RETURNS integer AS
$BODY$


DECLARE 

	--- Temporary values
	idx_channel			INTEGER;

	--- Catalog indices
	idx_annotation_channel			INTEGER DEFAULT 1;
		
BEGIN	

	--- Get annotation session type id
	SELECT INTO idx_annotation_channel signal.annotation_channel_type.id FROM signal.annotation_channel_type WHERE signal.annotation_channel_type.type_name = $5 AND signal.annotation_channel_type.subtype_name = $6;
	IF idx_annotation_channel = 0 OR idx_annotation_channel IS NULL THEN
		$8 = -1501;
		RETURN;
	END IF;

	BEGIN
		IF insert_annotation_channel THEN
			FOREACH idx_channel IN ARRAY ida_channel
			LOOP
				INSERT INTO signal.annotation_channel (
					event_start_utc,
					event_stop_utc,
					annotation_text,
					id_annotation_channel_type,
					id_channel) VALUES ($2, $3, $4, idx_annotation_channel, idx_channel); 
			END LOOP;
			$8 = 1;
		ELSE
			FOREACH idx_channel IN ARRAY ida_channel
			LOOP
				UPDATE signal.annotation_channel SET 
					event_start_utc = $2,
					event_stop_utc= $3,
					annotation_text = $4,
					id_annotation_channel_type = idx_annotation_channel
				WHERE signal.annotation_channel.id = idx_channel;
			END LOOP;
			$8 = 2;
		END IF;
	EXCEPTION WHEN OTHERS THEN
		$8 = -255;
	END;
	$8 = 1;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
