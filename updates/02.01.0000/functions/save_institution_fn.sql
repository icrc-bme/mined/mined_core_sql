CREATE OR REPLACE FUNCTION demographic.save_institution(
	IN id_institution integer,
    IN zone_name character varying,
    IN institution_name character varying,
    OUT id integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalog indices
	idx_time_zone		INTEGER;

BEGIN	

	--- Get time zone id
	SELECT INTO idx_time_zone demographic.time_zone.id FROM demographic.time_zone WHERE demographic.time_zone.zone_name = $2;
	IF idx_time_zone IS NULL THEN
		$4 = -101;
		RETURN;
	END IF;

	BEGIN
	IF $1 = 0 THEN
		INSERT INTO demographic.institution( id_time_zone, institution_name) VALUES( idx_time_zone, $3) RETURNING demographic.institution.id INTO $4;
	ELSE
		UPDATE demographic.institution SET id_time_zone = idx_time_zone,  institution_name = $3 WHERE  demographic.institution.id = $1;
		$4 = $1;
	END IF;
	EXCEPTION WHEN OTHERS THEN
		$4 = -255;
	END;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;