DROP FUNCTION IF EXISTS "space".upsert_coordinates;
CREATE OR REPLACE FUNCTION "space".upsert_coordinates(
	IN id_contact integer,
	IN c_x float,
    IN c_y float,
    IN c_z float,
    IN system_name character varying,
	IN system_unit character varying,
    OUT id integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalogs
	idx_coordinate_system			INTEGER default 1;

BEGIN

	--- Get coordinate system id
	IF system_name IS NOT NULL THEN
		SELECT INTO
			idx_coordinate_system "space".coordinate_system.id
		FROM
			"space".coordinate_system
		WHERE
			"space".coordinate_system.system_name = $5
			AND "space".coordinate_system.system_unit = $6;
		IF idx_coordinate_system IS NULL THEN
			INSERT INTO
				"space".coordinate_system(system_name, system_unit)
			VALUES
				($5, $6)
				RETURNING "space".coordinate_system.id INTO idx_coordinate_system;
		END IF;
	END IF;


	INSERT INTO "space".coordinates(
		c_x,
		c_y,
		c_z,
		id_coordinate_system,
		id_contact
		) 
	VALUES (
		$2,
		$3,
		$4,
		idx_coordinate_system,
		id_contact
		)
	ON CONFLICT ON CONSTRAINT coordinates_un DO UPDATE SET
		c_x = $2,
		c_y = $3,
		c_z = $4
	RETURNING
		"space".coordinates.id INTO $7;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;