CREATE OR REPLACE FUNCTION demographic.upsert_subject(
    IN id_institution integer,
    IN id_subject integer,
    IN subject_type character varying,
    IN sex character varying,
    IN handedness character varying,
    IN birth_date TIMESTAMP,
    IN seizure_onset_age real,
    IN note character varying,
    OUT id_mined integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalog indices
	idx_type		INTEGER default 1;
	idx_sex			INTEGER default 1;
	idx_handedness  INTEGER default 1;
	
BEGIN

	--- Get subject type id
	IF subject_type IS NOT NULL THEN
		SELECT INTO idx_type demographic.subject_type.id FROM demographic.subject_type WHERE demographic.subject_type.type_name = $3;
		IF idx_type IS NULL THEN
			INSERT INTO demographic.subject_type(type_name) VALUES ($3) RETURNING id INTO idx_type;
		END IF;
	END IF;

	--- Get sex id
	IF sex IS NOT NULL THEN
		SELECT INTO idx_sex demographic.sex.id FROM demographic.sex WHERE demographic.sex.sex_name = $4;
		IF idx_sex IS NULL THEN
			INSERT INTO demographic.sex(sex_name) VALUES ($4) RETURNING id INTO idx_sex;
		END IF;
	END IF;

	--- Get handedness id
	IF handedness IS NOT NULL THEN
		SELECT INTO idx_handedness demographic.handedness.id FROM demographic.handedness WHERE demographic.handedness.handedness_name = $5;
		IF idx_handedness IS NULL THEN
			INSERT INTO demographic.handedness(handedness_name) VALUES ($5) RETURNING id INTO idx_handedness;
		END IF;
	END IF;

	INSERT INTO demographic.subject(
		id_institution,
		id_subject,
		id_subject_type,
		id_sex,
		id_handedness,
		dt_birth,
		seizure_onset_age,
		note)
	VALUES (
		$1,
		$2,
		idx_type,
		idx_sex,
		idx_handedness,
		birth_date,
		$7,
		$8)
	ON CONFLICT ON CONSTRAINT subject_un DO UPDATE SET
		id_subject_type = idx_type,
		id_sex = idx_sex,
		id_handedness = idx_handedness,
		dt_birth = birth_date,
		seizure_onset_age = $7,
		note = $8
	RETURNING
		demographic.subject.id_mined INTO $9;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 
