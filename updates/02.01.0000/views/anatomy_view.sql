DROP VIEW IF EXISTS "space".anatomy_view;   
CREATE OR REPLACE VIEW "space".anatomy_view
AS SELECT a.id,
    a.hemisphere,
    a.brain_area,
    a.id_atlas_type,
    a.id_anatomy_structure,
    a.id_lobe,
    atl.type_name AS atlas_name,
    l.lobe_name,
    st.structure_name,
    ac.id_contact
   FROM space.anatomy a
     JOIN space.atlas_type atl ON a.id_atlas_type = atl.id
     JOIN space.lobe l ON a.id_lobe = l.id
     JOIN space.anatomy_structure st ON a.id_anatomy_structure = st.id
     JOIN space.anatomy_contact ac ON a.id = ac.id_anatomy;

