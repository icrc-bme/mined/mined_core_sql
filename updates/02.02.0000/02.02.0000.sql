-------------- DROP ALL VIEWS ------------

DO $$
DECLARE drop_query VARCHAR(512);
BEGIN
FOR drop_query IN (
	SELECT 'DROP VIEW ' || table_schema || '.' || table_name || ';'
		FROM information_schema.views
	WHERE table_schema IN ('demographic', 'recording', 'space', 'signal', 'bivariate', 'mined_user')
	   	AND table_name !~ '^pg_'
	)
	LOOP
		EXECUTE drop_query;
	END LOOP;
END $$;


-------------- DROP ALL FUNCTIONS ------------

DO $$
DECLARE drop_query VARCHAR(512);
BEGIN
FOR drop_query IN (
	SELECT  'DROP FUNCTION ' || routines.specific_schema || '.' || routines.routine_name || ' CASCADE;'
	FROM information_schema.routines
	    LEFT JOIN information_schema.parameters ON routines.specific_name=parameters.specific_name
	WHERE routines.specific_schema IN ('demographic', 'recording', 'space', 'signal', 'bivariate', 'mined_user')
	GROUP BY routines.specific_schema, routines.routine_name
	)
	LOOP
		EXECUTE drop_query;
	END LOOP;
END $$;

-- Setup IRB tables
CREATE TABLE demographic.irb (
	irb_num VARCHAR(16) NOT NULL,
	CONSTRAINT irb_pk PRIMARY KEY (irb_num)
);

CREATE TABLE demographic.irb_subject (
	id serial NOT NULL,
	id_mined_subject int4 NOT NULL,
	irb_num VARCHAR(16) NOT NULL,
	irb_date timestamp NULL,
	CONSTRAINT irb_sub_pk PRIMARY KEY (id),	
	CONSTRAINT irb_sub_un UNIQUE (id_mined_subject, irb_num),
	CONSTRAINT subject_fk FOREIGN KEY (id_mined_subject) REFERENCES demographic.subject(id_mined) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT irb_num_fk FOREIGN KEY (irb_num) REFERENCES demographic.irb(irb_num) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE
);

-- Create subject category table
CREATE TABLE demographic.subject_category (
	id serial NOT NULL,
	category_name VARCHAR(128) NOT NULL,
	CONSTRAINT subject_category_pk PRIMARY KEY (id),
	CONSTRAINT subject_category_un UNIQUE (category_name)
);

-- Populate subject category table
INSERT INTO demographic.subject_category (category_name) VALUES ('n/a'), ('Control'), ('Epilepsy'), ('Artic Sun'), ('Movement Disorders'), ('Pain');

-- Change types to fit the Mayo data
ALTER TABLE demographic.ethnicity 		ALTER COLUMN ethnicity_name TYPE VARCHAR(128);
ALTER TABLE demographic.race	  		ALTER COLUMN race_name 		TYPE VARCHAR(128);
ALTER TABLE demographic.subject_type	ALTER COLUMN type_name 		TYPE VARCHAR(128); 	-- currently varchar(1024) which is a little too much

-- Adjust columns in subject table
ALTER TABLE demographic.subject DROP COLUMN dt_birth;
ALTER TABLE demographic.subject ADD  COLUMN id_subject_category int4 NOT NULL DEFAULT 1;
ALTER TABLE demographic.subject ADD CONSTRAINT category_fk FOREIGN KEY (id_subject_category) REFERENCES demographic.subject_category(id) ON UPDATE CASCADE ON DELETE SET NULL;

-------------- CORRECT CONSTRAINTS IN BASELINE TABLES ------------

-- First, drop the existing foreign key constraints
ALTER TABLE demographic.institution 				DROP CONSTRAINT time_zone_fk; 
ALTER TABLE demographic.subject 					DROP CONSTRAINT handedness_fk;
ALTER TABLE demographic.subject 					DROP CONSTRAINT sex_fk;
ALTER TABLE demographic.subject 					DROP CONSTRAINT subject_type_fk;
ALTER TABLE demographic.subject_histopathologies 	DROP CONSTRAINT subject_fk;
ALTER TABLE demographic.subject_outcomes 			DROP CONSTRAINT subject_fk;
ALTER TABLE "space".anatomy 						DROP CONSTRAINT anatomy_structure_fk;
ALTER TABLE "space".anatomy 						DROP CONSTRAINT atlas_type_fk;
ALTER TABLE "space".anatomy 						DROP CONSTRAINT lobe_fk;
ALTER TABLE recording."session" 					DROP CONSTRAINT modality_fk;
ALTER TABLE recording."session" 					DROP CONSTRAINT recording_reference_fk;
ALTER TABLE recording."session" 					DROP CONSTRAINT session_type_fk;
ALTER TABLE recording."session" 					DROP CONSTRAINT subject_fk;
ALTER TABLE "space".contact 						DROP CONSTRAINT contact_size_fk;
ALTER TABLE "space".contact 						DROP CONSTRAINT contact_type_fk;
ALTER TABLE "space".contact 						DROP CONSTRAINT imaging_pathology_fk;
ALTER TABLE "space".contact_subjects_interventions 	DROP CONSTRAINT contact_fk;
ALTER TABLE "space".contact_subjects_interventions 	DROP CONSTRAINT subject_interventions_fk;
ALTER TABLE "space".anatomy_contact 				DROP CONSTRAINT anatomy_fk;
ALTER TABLE "space".anatomy_contact 				DROP CONSTRAINT contact_fk;
ALTER TABLE recording.trial 						DROP CONSTRAINT trial_type_fk;
ALTER TABLE recording.annotation_session 			DROP CONSTRAINT session_fk;
ALTER TABLE util."_bids_entities" 					DROP CONSTRAINT "_bids_entities_fk";
ALTER TABLE signal.channel 							DROP CONSTRAINT channel_type_fk;
ALTER TABLE signal.channel 							DROP CONSTRAINT quality_type_fk;

-- Then, recreate the foreign key constraints with correct ON DELETE policy
ALTER TABLE "space".anatomy ALTER COLUMN id_anatomy_structure 	SET DEFAULT 1;
ALTER TABLE "space".anatomy ALTER COLUMN id_atlas_type 			SET DEFAULT 1;
ALTER TABLE "space".anatomy ALTER COLUMN id_lobe 				SET DEFAULT 1;
ALTER TABLE recording.trial ALTER COLUMN id_trial_type 			SET DEFAULT 1;

ALTER TABLE demographic.institution 				ADD CONSTRAINT time_zone_fk 			FOREIGN KEY (id_time_zone) 				REFERENCES demographic.time_zone(id) 				MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE demographic.subject 					ADD CONSTRAINT handedness_fk 			FOREIGN KEY (id_handedness) 			REFERENCES demographic.handedness(id) 				MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE demographic.subject 					ADD CONSTRAINT sex_fk 					FOREIGN KEY (id_sex) 					REFERENCES demographic.sex(id) 						MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE demographic.subject 					ADD CONSTRAINT subject_type_fk 			FOREIGN KEY (id_subject_type) 			REFERENCES demographic.subject_type(id) 			MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE demographic.subject_histopathologies 	ADD CONSTRAINT subject_fk 				FOREIGN KEY (id_mined_subject) 			REFERENCES demographic.subject(id_mined) 			MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE demographic.subject_outcomes 			ADD CONSTRAINT subject_fk 				FOREIGN KEY (id_mined_subject) 			REFERENCES demographic.subject(id_mined) 			MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE "space".anatomy 						ADD CONSTRAINT anatomy_structure_fk 	FOREIGN KEY (id_anatomy_structure) 		REFERENCES "space".anatomy_structure(id) 			MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE "space".anatomy 						ADD CONSTRAINT atlas_type_fk 			FOREIGN KEY (id_atlas_type) 			REFERENCES "space".atlas_type(id) 					MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE "space".anatomy 						ADD CONSTRAINT lobe_fk 					FOREIGN KEY (id_lobe) 					REFERENCES "space".lobe(id) 						MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE recording."session" 					ADD CONSTRAINT modality_fk 				FOREIGN KEY (id_modality) 				REFERENCES recording.modality(id) 					MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE recording."session" 					ADD CONSTRAINT recording_reference_fk 	FOREIGN KEY (id_recording_reference) 	REFERENCES signal.recording_reference(id) 			MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE recording."session" 					ADD CONSTRAINT session_type_fk 			FOREIGN KEY (id_session_type) 			REFERENCES recording.session_type(id) 				MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE recording."session" 					ADD CONSTRAINT subject_fk	 			FOREIGN KEY (id_mined_subject) 			REFERENCES demographic.subject(id_mined) 			MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE "space".contact 						ADD CONSTRAINT contact_size_fk 			FOREIGN KEY (id_contact_size) 			REFERENCES "space".contact_size(id) 				MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE "space".contact 						ADD CONSTRAINT contact_type_fk 			FOREIGN KEY (id_contact_type) 			REFERENCES "space".contact_type(id) 				MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE "space".contact 						ADD CONSTRAINT imaging_pathology_fk 	FOREIGN KEY (id_imaging_pathology) 		REFERENCES "space".imaging_pathology(id) 			MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE "space".contact_subjects_interventions 	ADD CONSTRAINT contact_fk 				FOREIGN KEY (id_contact) 				REFERENCES "space".contact(id) 						MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE "space".contact_subjects_interventions 	ADD CONSTRAINT subject_interventions_fk	FOREIGN KEY (id_subject_intervention) 	REFERENCES demographic.subject_interventions(id) 	MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE "space".anatomy_contact 				ADD CONSTRAINT anatomy_fk 				FOREIGN KEY (id_anatomy) 				REFERENCES "space".anatomy(id) 						MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE "space".anatomy_contact 				ADD CONSTRAINT contact_fk 				FOREIGN KEY (id_contact) 				REFERENCES "space".contact(id) 						MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE recording.trial 						ADD CONSTRAINT trial_type_fk 			FOREIGN KEY (id_trial_type) 			REFERENCES recording.trial_type(id) 				MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE recording.annotation_session 			ADD CONSTRAINT session_fk 				FOREIGN KEY (id_session) 				REFERENCES recording."session"(id) 					MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE util."_bids_entities" 					ADD CONSTRAINT "_bids_entities_fk" 		FOREIGN KEY (id) 						REFERENCES recording."session"(id) 					MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE signal.channel 							ADD CONSTRAINT channel_type_fk 			FOREIGN KEY (id_channel_type) 			REFERENCES signal.channel_type(id) 					MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE signal.channel 							ADD CONSTRAINT quality_type_fk 			FOREIGN KEY (id_quality_type) 			REFERENCES signal.quality_type(id) 					MATCH FULL ON UPDATE CASCADE ON DELETE SET DEFAULT;