# Update overview
This update implements changes that satisfy (some) requirements from the Mayo Clinic, allowing better transfer from the current database used on this institution. Some discovered bugs from the previous versions have been fixed here as well.


# Fixed bugs
- function ``insert_subject_histopathologies_fn.sql`` have been previously selecting incorrect histopathology type, not considering histopathology subtypes. This has been fixed.
- same case for function ``upsert_subject_histopathologies_fn.sql``


# Introduced changes
- column ``dt_birth`` removed from table ``demographic.subject`` as it is not GDPR compliant
- added new table ``demographic.irb_subject`` which stores information about IRB approval for particular patients
- added new table ``demographic.irb`` which stores available IRB numbers
- added ``irb_view`` displaying all subjects with IRB approval (one subject can have more approvals assigned)
- added new table ``demographic.subject_category`` categorizing subjects according to their treated diagnosis (e.g. epilepsy, control, pain, etc.)
- ``subject_category`` column added to ``subject_view.sql``
- added a new column to the ``demographic.subject`` with id pointing out to the table ``demographic.subject_category``
- table ``demographic.subject_category`` filled with categories according to the current database used at Mayo Clinic
- A few columns have changed datatypes/length to accommodate categorical data from the current database used at Mayo Clinic (see the ``02.02.0000.sql`` file for details)