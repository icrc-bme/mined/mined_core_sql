CREATE OR REPLACE FUNCTION demographic.insert_subject_histopathologies(
	id_mined integer,
	date_h timestamp,
	histopathology_type character varying,
	histopathology_subtype character varying,
	OUT out_id_h integer)
 RETURNS integer AS

$BODY$

DECLARE 

	--- Catalog indices
	idx_histopathology		INTEGER DEFAULT 1;

BEGIN	
	--- Get histopathology type id
	IF histopathology_type IS NOT NULL THEN
		SELECT INTO idx_histopathology demographic.histopathology.id FROM demographic.histopathology WHERE demographic.histopathology.histopathology_type = $3 AND demographic.histopathology.histopathology_subtype = $4;
		IF idx_histopathology IS NULL THEN
			INSERT INTO demographic.histopathology(histopathology_type, histopathology_subtype) VALUES ($3, $4) RETURNING id INTO idx_histopathology;
		END IF;
	END IF;

	INSERT INTO demographic.subject_histopathologies(
		dt_date_utc,
		id_histopathology,
		id_mined_subject) 
	VALUES (
		$2,
		idx_histopathology,
		$1)
	RETURNING
		demographic.subject_histopathologies.id INTO $5;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
