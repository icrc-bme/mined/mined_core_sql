CREATE OR REPLACE FUNCTION recording.save_annotation_session(
	IN id_annotation_session integer,
	IN event_start_utc TIMESTAMP,
    IN id_session integer,
    IN event_stop_utc TIMESTAMP,
    IN annotation_text character varying,
    IN annotation_type character varying,
    IN annotation_subtype character varying,
    OUT id_annotation integer)
 RETURNS integer AS
$BODY$

DECLARE 

	--- Catalog indices
	idx_annotation_session			INTEGER DEFAULT 1;

BEGIN	

	--- Get annotation session type id
	SELECT INTO idx_annotation_session recording.annotation_session_type.id FROM recording.annotation_session_type WHERE recording.annotation_session_type.type_name = $6 AND recording.annotation_session_type.subtype_name = $7;
	IF idx_annotation_session = 0 OR idx_annotation_session IS NULL THEN
		$8 = -1401;
		RETURN;
	END IF;

	BEGIN
		IF $1 = 0 THEN
			INSERT INTO recording.annotation_session(
				event_start_utc,
				id_session,
				event_stop_utc,
				annotation_text,
				id_annotation_session_type) VALUES( $2, $3, $4, $5, idx_annotation_session) RETURNING recording.annotation_session.id INTO $8;
		ELSE
			UPDATE recording.annotation_session SET 
				event_start_utc = $2,
				id_session = $3,
				event_stop_utc = $4,
				annotation_text = $5,
				id_annotation_session_type = idx_annotation_session
			WHERE  recording.annotation_session.id = $1;
			$8 = $1;
		END IF;
	EXCEPTION WHEN OTHERS THEN
		$8 = -255;
	END;

	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

