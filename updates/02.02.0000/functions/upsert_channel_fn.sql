CREATE OR REPLACE FUNCTION signal.upsert_channel(
	IN channel_name character varying,
	IN id_session integer,
	IN sampling_frequency float,
	IN low_cutoff float,
	IN high_cutoff float,
	IN unit character varying,
	IN acquisition_number integer,
	IN quality_type character varying,
	IN channel_type character varying,
	IN recording_reference character varying,
	IN ida_contact BIGINT[],
    OUT out_id_channel integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalogs
	idx_quality_type			INTEGER default 1;
	idx_channel_type			INTEGER default 1;
	idx_recording_reference		INTEGER default 1;

	--- Temporary values
	idx_contact				INTEGER;

BEGIN	

	--- Get quality type id
	IF quality_type IS NOT NULL THEN
		SELECT INTO idx_quality_type signal.quality_type.id FROM signal.quality_type WHERE signal.quality_type.type_name = $8;
		IF idx_quality_type IS NULL THEN
			INSERT INTO signal.quality_type(type_name) VALUES ($8) RETURNING signal.quality_type.id INTO idx_quality_type;
		END IF;
	END IF;

	--- Get channel type id
	IF channel_type IS NOT NULL THEN
		SELECT INTO idx_channel_type signal.channel_type.id FROM signal.channel_type WHERE signal.channel_type.type_name = $9;
		IF idx_channel_type IS NULL THEN
			INSERT INTO signal.channel_type(type_name) VALUES ($9) RETURNING signal.channel_type.id INTO idx_channel_type;
		END IF;
	END IF;

	-- Get recording reference id
	IF recording_reference IS NOT NULL THEN
		SELECT INTO idx_recording_reference signal.recording_reference.id FROM signal.recording_reference WHERE signal.recording_reference.reference_name = $10;
		IF idx_recording_reference IS NULL THEN
			INSERT INTO signal.recording_reference(reference_name) VALUES ($10) RETURNING signal.recording_reference.id INTO idx_recording_reference;
		END IF;
	END IF;
	
	INSERT INTO signal.channel(
		channel_name,
		sampling_frequency,
		id_quality_type,
		id_channel_type,
		id_session,
		low_cutoff,
		high_cutoff,
		unit,
		acquisition_number)
	VALUES (
		$1,
		$3,
		idx_quality_type,
		idx_channel_type,
		$2,
		$4,
		$5,
		$6,
		$7)
	ON CONFLICT ON CONSTRAINT channel_un DO UPDATE SET
		sampling_frequency = $3,
		id_quality_type = idx_quality_type,
		id_channel_type = idx_channel_type,
		low_cutoff = $4,
		high_cutoff = $5,
		unit = $6,
		acquisition_number = $7
	RETURNING
		signal.channel.id INTO $12;
	
	DELETE FROM signal.channel_contact WHERE signal.channel_contact.id_channel = $12;

	IF ida_contact IS NOT NULL THEN
		FOREACH idx_contact IN ARRAY ida_contact
		LOOP
			INSERT INTO signal.channel_contact (id_channel, id_contact, id_recording_reference) VALUES ($12, idx_contact, idx_recording_reference); 
		END LOOP;
	END IF;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;