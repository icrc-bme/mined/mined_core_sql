DROP VIEW  IF EXISTS signal.channel_contact_view;            
CREATE OR REPLACE VIEW signal.channel_contact_view
AS SELECT cc.id,
    c2.id AS id_channel,
    c2.channel_name,
    c.contact_number,
    e.electrode_name,
    c3.c_x,
    c3.c_y,
    c3.c_z,
    cs.system_name,
    cs.system_unit,
    c2.id_session
   FROM signal.channel_contact cc
     JOIN space.contact c ON cc.id_contact = c.id
     JOIN signal.channel c2 ON cc.id_channel = c2.id
     JOIN space.coordinates c3 ON c.id = c3.id_contact
     JOIN space.coordinate_system cs ON c3.id_coordinate_system = cs.id    
     JOIN space.electrode e ON e.id = c.id_electrode; 