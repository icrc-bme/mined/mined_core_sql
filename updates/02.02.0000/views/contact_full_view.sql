CREATE OR REPLACE VIEW "space".contact_full_view
AS SELECT c.id AS id_contact,
    s.id_mined,
    c.id_electrode,
    c.contact_number,
    c.impedance,
    ip.pathology_name,
    cs2.size_name AS contact_size,
    cs2.size_value AS contact_size_value,
    ct.type_name AS contact_type,
    e.electrode_name,
    et.type_name AS electrode_type,
    et.manufacturer AS electrode_manuf,
    et.contact_material,
    et.macro_contact_distance,
    et.micro_contact_distance,
    et.dimension_a,
    et.dimension_b,
    ac.manual_assign,
    a.brain_area,
    a.hemisphere,
    alt.type_name AS atlas_name,
    l.lobe_name,
    ans.structure_name,
    cs.system_name,
    cs.system_unit,
    c2.c_x,
    c2.c_y,
    c2.c_z
   FROM space.contact c
     LEFT JOIN space.coordinates c2 ON c.id = c2.id_contact
     LEFT JOIN space.coordinate_system cs ON c2.id_coordinate_system = cs.id
     LEFT JOIN space.anatomy_contact ac ON c.id = ac.id_contact
     LEFT JOIN space.anatomy a ON ac.id_anatomy = a.id
     LEFT JOIN space.atlas_type alt ON a.id_atlas_type = alt.id
     LEFT JOIN space.lobe l ON a.id_lobe = l.id
     LEFT JOIN space.anatomy_structure ans ON a.id_anatomy_structure = ans.id
     LEFT JOIN space.electrode e ON c.id_electrode = e.id
     LEFT JOIN space.electrode_type et ON e.id_electrode_type = et.id
     LEFT JOIN demographic.subject s ON e.id_mined_subject = s.id_mined
     LEFT JOIN space.imaging_pathology ip ON c.id_imaging_pathology = ip.id
     LEFT JOIN space.contact_size cs2 ON c.id_contact_size = cs2.id
     LEFT JOIN space.contact_type ct ON c.id_contact_type = ct.id;
