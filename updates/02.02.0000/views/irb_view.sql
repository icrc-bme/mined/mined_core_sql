DROP VIEW  IF EXISTS demographic.irb_view;     
CREATE OR REPLACE VIEW demographic.irb_view 
AS SELECT irb.irb_num, 
    irb.irb_date, 
    s.id_subject
FROM demographic.irb_subject irb
JOIN demographic.subject s ON irb.id_mined_subject  = s.id_mined;
