CREATE OR REPLACE VIEW "space".structure_lobe_view
AS SELECT a.id_anatomy_structure,
    l.lobe_name,
    l.id AS id_lobe
   FROM space.anatomy a
     JOIN space.lobe l ON a.id_lobe = l.id;
