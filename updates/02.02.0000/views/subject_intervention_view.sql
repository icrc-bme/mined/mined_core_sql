DROP VIEW IF EXISTS  demographic.subject_intervention_view;    
CREATE OR REPLACE VIEW demographic.subject_intervention_view
AS SELECT si.id AS id_subject_intervention,
    si.id_mined_subject,
    i.intervention_name,
    si.dt_date_utc,
    tz.zone_name
   FROM demographic.subject_interventions si
     JOIN demographic.intervention i ON si.id_intervention = i.id
     JOIN demographic.subject s ON si.id_mined_subject = s.id_mined
     JOIN demographic.institution it ON s.id_institution = it.id
     JOIN demographic.time_zone tz ON it.id_time_zone = tz.id;  

