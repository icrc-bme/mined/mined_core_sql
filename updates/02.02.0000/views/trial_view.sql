DROP VIEW  IF EXISTS recording.trial_view;         
CREATE OR REPLACE VIEW recording.trial_view
AS SELECT t.id,
    s.id AS id_session,
    ans.event_start_utc,
    ans.event_stop_utc,
    ast.type_name AS annotation_type,
    ast.subtype_name AS annotation_subtype,
    t.response_time,
    t.response,
    t.ttl_value,
    tt.type_name AS trial_type,
    t2.task_name,
    t2.task_description,
    t2.patient_instructions
   FROM recording.session s
     JOIN recording.annotation_session ans ON ans.id_annotation_session_type = s.id
     JOIN recording.annotation_session_type ast ON ans.id_annotation_session_type = ast.id
     JOIN recording.trial t ON t.id_annotation_session = ans.id
     JOIN recording.trial_type tt ON t.id_trial_type = tt.id
     JOIN recording.task t2 ON tt.id_task = t2.id;    
