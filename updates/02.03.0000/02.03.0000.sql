-------------- DROP ALL VIEWS ------------

DO $$
DECLARE drop_query VARCHAR(512);
BEGIN
FOR drop_query IN (
	SELECT 'DROP VIEW ' || table_schema || '.' || table_name || ';'
		FROM information_schema.views
	WHERE table_schema IN ('demographic', 'recording', 'space', 'signal', 'bivariate', 'mined_user')
	   	AND table_name !~ '^pg_'
	)
	LOOP
		EXECUTE drop_query;
	END LOOP;
END $$;


-------------- DROP ALL FUNCTIONS ------------

DO $$
DECLARE drop_query VARCHAR(512);
BEGIN
FOR drop_query IN (
	SELECT  'DROP FUNCTION ' || routines.specific_schema || '.' || routines.routine_name || ' CASCADE;'
	FROM information_schema.routines
	    LEFT JOIN information_schema.parameters ON routines.specific_name=parameters.specific_name
	WHERE routines.specific_schema IN ('demographic', 'recording', 'space', 'signal', 'bivariate', 'mined_user')
	GROUP BY routines.specific_schema, routines.routine_name
	)
	LOOP
		EXECUTE drop_query;
	END LOOP;
END $$;

-- Add patient_prefix column to demographic.institution table
ALTER TABLE demographic.institution ADD COLUMN patient_prefix VARCHAR(8) NULL;

-- Fill in missing patient_prefix values
UPDATE demographic.institution SET patient_prefix = '';

-- Add NOT NULL constraint to patient_prefix column
ALTER TABLE demographic.institution ALTER COLUMN patient_prefix SET NOT NULL;