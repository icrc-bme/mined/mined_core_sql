CREATE OR REPLACE FUNCTION "space".insert_anatomy(
	IN hemisphere bool,
    IN atlas_name character varying,
	IN anat_structure_name character varying,
	IN anat_lobe_name character varying,
	IN brain_area integer,
    OUT id integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalogs
	idx_atlas				INTEGER default 1;
	idx_structure			INTEGER default 1;
	idx_lobe				INTEGER default 1;

BEGIN

	--- Get atlas id
	IF atlas_name IS NOT NULL THEN
		SELECT INTO
			idx_atlas "space".atlas_type.id
		FROM
			"space".atlas_type
		WHERE
			"space".atlas_type.type_name ILIKE $2;
		IF idx_atlas IS NULL THEN
			INSERT INTO
				"space".atlas_type(type_name)
			VALUES
				($2)
			RETURNING "space".atlas_type.id INTO idx_atlas;
		END IF;
	END IF;

	--- Get structure id
	IF anat_structure_name IS NOT NULL THEN
		SELECT INTO
			idx_structure "space".anatomy_structure.id
		FROM
			"space".anatomy_structure
		WHERE
			"space".anatomy_structure.structure_name ILIKE $3;
		IF idx_structure IS NULL THEN
			INSERT INTO
				"space".anatomy_structure(structure_name)
			VALUES
				($3)
			RETURNING "space".anatomy_structure.id INTO idx_structure;
		END IF;
	END IF;

	--- Get lobe id
	IF anat_lobe_name IS NOT NULL THEN
		SELECT INTO
			idx_lobe "space".lobe.id
		FROM
			"space".lobe
		WHERE
			"space".lobe.lobe_name ILIKE $4;
		IF idx_lobe IS NULL THEN
			INSERT INTO
				"space".lobe(lobe_name)
			VALUES
				($4)
			RETURNING "space".lobe.id INTO idx_lobe;
		END IF;
	END IF;

	INSERT INTO "space".anatomy(
		hemisphere,
		brain_area,
		id_atlas_type,
		id_anatomy_structure,
		id_lobe
		) 
	VALUES (
		$1,
		$5,
		idx_atlas,
		idx_structure,
		idx_lobe
		)
	RETURNING
		"space".anatomy.id INTO $6;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;