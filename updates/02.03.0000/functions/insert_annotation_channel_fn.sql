CREATE OR REPLACE FUNCTION signal.insert_annotation_channel(
    IN event_start_utc TIMESTAMP,
    IN id_channel integer,
    IN event_stop_utc TIMESTAMP,
    IN annotation_text character varying,
    IN annotation_type character varying,
	IN annotation_subtype character varying,
    OUT id_annotation integer)
  RETURNS integer AS
$BODY$

DECLARE 

    --- Catalog indices
	idx_annotation_channel			INTEGER DEFAULT 1;

BEGIN

    --- Get annotation channel type id
	SELECT INTO idx_annotation_channel signal.annotation_channel_type.id FROM signal.annotation_channel_type WHERE signal.annotation_channel_type.type_name ILIKE $5 AND signal.annotation_channel_type.subtype_name ILIKE $6;
	IF idx_annotation_channel = 0 OR idx_annotation_channel IS NULL THEN
		$7 = -1501;
		RETURN;
	END IF;

	INSERT INTO signal.annotation_channel(
		event_start_utc,
		event_stop_utc,
	    annotation_text,
	    id_annotation_channel_type,
		id_channel)
	VALUES (
		$1,
		$3,
		$4,
		idx_annotation_channel,
	    $2)
	RETURNING
		signal.annotation_channel.id INTO $7;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 
