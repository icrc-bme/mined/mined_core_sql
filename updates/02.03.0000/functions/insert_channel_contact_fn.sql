CREATE OR REPLACE FUNCTION signal.insert_channel_contact(
	IN id_channel integer,
	IN id_contact integer,
	IN recording_reference character varying,
    OUT id integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalogs
	idx_recording_reference		INTEGER default 0;

BEGIN

	-- Get recording reference id
	IF recording_reference IS NOT NULL THEN
		SELECT INTO idx_recording_reference signal.recording_reference.id FROM signal.recording_reference WHERE signal.recording_reference.reference_name ILIKE $3;
		IF idx_recording_reference IS NULL THEN
			INSERT INTO signal.recording_reference(reference_name) VALUES ($3) RETURNING signal.recording_reference.id INTO idx_recording_reference;
		END IF;
	END IF;


	INSERT INTO signal.channel_contact(
		id_channel,
		id_contact,
		id_recording_reference) 
	VALUES (
		$1,
		$2,
		idx_recording_reference)
	RETURNING
		signal.channel_contact.id INTO $4;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;