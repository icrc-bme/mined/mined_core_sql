CREATE OR REPLACE FUNCTION "space".save_contact_anatomy(
	IN id_anatomy integer,
	IN hemisphere bool,
  IN atlas_name character varying,
	IN anat_structure_name character varying,
	IN anat_lobe_name character varying,
	IN brain_area integer,
	IN ida_contact BIGINT[],
	IN manual bool,
	OUT out_id_anatomy integer)
 RETURNS integer AS
$BODY$

DECLARE 

	--- Catalogs
	idx_atlas				INTEGER default 1;
	idx_structure			INTEGER default 1;
	idx_lobe				INTEGER default 1;

	--- Temporary values
	idx_anatomy				INTEGER default 0;
	idx_contact				INTEGER;

BEGIN

	--- Get atlas id
	IF atlas_name IS NOT NULL THEN
		SELECT INTO
			idx_atlas "space".atlas_type.id
		FROM
			"space".atlas_type
		WHERE
			"space".atlas_type.type_name ILIKE $3;
		IF idx_atlas IS NULL THEN
			$9 = -1101;
			RETURN;
		END IF;
	END IF;

	--- Get structure id
	IF anat_structure_name IS NOT NULL THEN
		SELECT INTO
			idx_structure "space".anatomy_structure.id
		FROM
			"space".anatomy_structure
		WHERE
			"space".anatomy_structure.structure_name ILIKE $4;
		IF idx_structure IS NULL THEN
			$9 = -1102;
			RETURN;
		END IF;
	END IF;

	--- Get lobe id
	IF anat_lobe_name IS NOT NULL THEN
		SELECT INTO
			idx_lobe "space".lobe.id
		FROM
			"space".lobe
		WHERE
			"space".lobe.lobe_name ILIKE $5;
		IF idx_lobe IS NULL THEN
			$9 = -1103;
			RETURN;
		END IF;
	END IF;

	BEGIN
		IF $1 = 0 THEN
			INSERT INTO "space".anatomy(
				hemisphere,
				brain_area,
				id_atlas_type,
				id_anatomy_structure,
				id_lobe
				) 
			VALUES (
				$2,
				$6,
				idx_atlas,
				idx_structure,
				idx_lobe
				)
			RETURNING
				"space".anatomy.id INTO idx_anatomy;

			IF ida_contact IS NOT NULL THEN
				FOREACH idx_contact IN ARRAY ida_contact
				LOOP
					INSERT INTO "space".anatomy_contact (id_anatomy, id_contact, manual_assign ) VALUES (idx_anatomy, idx_contact, manual); 
				END LOOP;
			END IF;
			$9 = idx_anatomy;

		ELSE

			UPDATE "space".anatomy SET 
				hemisphere = $2,
				brain_area = $6,
				id_atlas_type = idx_atlas,
				id_anatomy_structure = idx_structure,
				id_lobe = idx_lobe
			WHERE "space".anatomy.id=$1;

			IF ida_contact IS NOT NULL THEN
				FOREACH idx_contact IN ARRAY ida_contact
				LOOP
					INSERT INTO "space".anatomy_contact (id_anatomy, id_contact, manual_assign ) VALUES ($1, idx_contact, manual); 
				END LOOP;
			END IF;
			$9 = $1;
		END IF;
		EXCEPTION WHEN OTHERS THEN
			$9 = -255;
	END;

RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

