CREATE OR REPLACE FUNCTION "space".save_coordinates(
	IN id_coordinates integer,
	IN id_contact integer,
	IN c_x float,
    IN c_y float,
    IN c_z float,
    IN system_name character varying,
	IN system_unit character varying,
    OUT out_id_contact integer)
  RETURNS integer AS
$BODY$


DECLARE 

	--- Catalogs
	idx_coordinate_system			INTEGER default 1;

BEGIN

	--- Get coordinate system id
	IF system_name IS NOT NULL THEN
		SELECT INTO
			idx_coordinate_system "space".coordinate_system.id
		FROM
			"space".coordinate_system
		WHERE
			"space".coordinate_system.system_name ILIKE $6
			AND "space".coordinate_system.system_unit = $7;
		IF idx_coordinate_system IS NULL THEN
			$8 = -1001;
			RETURN;
		END IF;
	END IF;


	BEGIN
	IF $1 = 0 THEN
		INSERT INTO "space".coordinates(
			c_x,
			c_y,
			c_z,
			id_coordinate_system,
			id_contact
			) 
		VALUES (
			$3,
			$4,
			$5,
			idx_coordinate_system,
			$2
			)
		RETURNING
			"space".coordinates.id INTO $8;
	ELSE
		UPDATE "space".coordinates SET 
			c_x = $3,
			c_y = $4,
			c_z = $5,
			id_coordinate_system = idx_coordinate_system,
			id_contact = $2
		WHERE  "space".coordinates.id = $1;
		$8 = $1;
	END IF;
	EXCEPTION WHEN OTHERS THEN
		$8 = -255;
	END;
	
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
