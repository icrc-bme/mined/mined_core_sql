CREATE OR REPLACE FUNCTION "space".save_electrode(
    IN id_electrode integer,
    IN electrode_name character varying,
    IN id_mined integer,
		IN type_name character varying,
		IN manufacturer character varying,
		IN dimension_a float,
    IN dimension_b float,
    IN contact_material character varying,
    IN macro_contact_distance float,
    IN micro_contact_distance float,
    OUT id integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalogs
	idx_electrode_type			INTEGER default 1;


BEGIN	

	--- Get electrode_type id
	SELECT INTO
		idx_electrode_type "space".electrode_type.id
	FROM
		"space".electrode_type
	WHERE
		"space".electrode_type.type_name ILIKE $5
		AND "space".electrode_type.manufacturer ILIKE $6
		AND "space".electrode_type.dimension_a = $7
		AND "space".electrode_type.dimension_b = $8;
	IF idx_electrode_type IS NULL THEN
		$11 = -801;
		RETURN;
	END IF;


	BEGIN
	IF $1 = 0 THEN
		INSERT INTO "space".electrode(
			electrode_name,
			id_electrode_type,
			id_mined_subject)
		VALUES (
			$2,
			idx_electrode_type,
			$3)
			RETURNING
		"space".electrode.id INTO $11;
	ELSE
		UPDATE "space".electrode SET 
			electrode_name = $2,
			id_electrode_type = idx_electrode_type,
			id_mined_subject = $3
		WHERE  "space".electrode.id = $1;
		$11 = $1;
	END IF;
	EXCEPTION WHEN OTHERS THEN
		$11 = -255;
	END;
	
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
