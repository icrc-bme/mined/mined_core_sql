CREATE OR REPLACE FUNCTION demographic.save_institution(
	IN id_institution integer,
    IN zone_name character varying,
    IN institution_name character varying,
    IN patient_prefix character varying,
    OUT id integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalog indices
	idx_time_zone		INTEGER;

BEGIN	

	--- Get time zone id
	SELECT INTO idx_time_zone demographic.time_zone.id FROM demographic.time_zone WHERE demographic.time_zone.zone_name = $2;
	IF idx_time_zone IS NULL THEN
		$5 = -101;
		RETURN;
	END IF;

	BEGIN
	IF $1 = 0 THEN
		INSERT INTO demographic.institution( id_time_zone, institution_name, patient_prefix) VALUES( idx_time_zone, $3, $4) RETURNING demographic.institution.id INTO $5;
	ELSE
		UPDATE demographic.institution SET id_time_zone = idx_time_zone,  institution_name = $3,  patient_prefix = $4 WHERE  demographic.institution.id = $1;
		$5 = $1;
	END IF;
	EXCEPTION WHEN OTHERS THEN
		$5 = -255;
	END;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;