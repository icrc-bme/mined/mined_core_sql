CREATE OR REPLACE FUNCTION recording.save_session(
    IN id_session integer,
    IN id_mined_subject integer,
	  IN dt_start_utc timestamp,
	  IN dt_stop_utc timestamp,
    IN acquisition_device character varying,
    IN ip_address character varying,
    IN session_name character varying,
    IN note character varying,
    IN session_type character varying,
    IN modality character varying,
    IN recording_reference character varying,
    OUT id_mined integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalogs
	idx_session_type			INTEGER default 1;
	idx_modality				INTEGER default 1;
	idx_recording_reference		INTEGER default 1;

	--- Temporary values
	idx_session				INTEGER default 0;
	idx_electrode				INTEGER default 0;

BEGIN	

	--- Get session type id
	IF $9 IS NOT NULL THEN
		SELECT INTO idx_session_type recording.session_type.id FROM recording.session_type WHERE recording.session_type.type_name ILIKE $9;
		IF idx_session_type IS NULL THEN
			$12=-601;
			RETURN;
		END IF;
	END IF;

	--- Get modality id
	IF $10 IS NOT NULL THEN
		SELECT INTO idx_modality recording.modality.id FROM recording.modality WHERE recording.modality.modality_name ILIKE $10;
		IF idx_modality IS NULL THEN
			$12=-602;
			RETURN;
		END IF;
	END IF;

	-- Get recording reference id
	IF $11 IS NOT NULL THEN
		SELECT INTO idx_recording_reference recording.recording_reference.id FROM recording.recording_reference WHERE recording.recording_reference.reference_name ILIKE $11;
		IF idx_recording_reference IS NULL THEN
			$12=-603;
			RETURN;
		END IF;
	END IF;

	BEGIN
		IF $1 = 0 THEN
			INSERT INTO recording.session(
				dt_start_utc,
				dt_stop_utc,
				acquisition_device,
				ip_address,
				session_name,
				note,
				id_session_type,
				id_modality,
				id_recording_reference,
				id_mined_subject) 
			VALUES (
				$3,
				$4,
				$5,
				$6,
				$7,
				$8,
				idx_session_type,
				idx_modality,
				idx_recording_reference,
				$2)
				RETURNING
				recording.session.id INTO idx_session;

		ELSE

			DELETE FROM recording.session_contact WHERE recording.session_contact.id_session = $1;

			UPDATE recording.session SET 
				dt_start_utc = $3,
				dt_stop_utc = $4,
				acquisition_device = $5,
				ip_address = $6,
				session_name = $7,
				note = $8,
				id_session_type = idx_session_type,
				id_modality = idx_modality,
				id_recording_reference = idx_recording_reference,
				id_mined_subject = $2
			WHERE  recording.session.id = $1;

			$12 = $1;
		END IF;
		EXCEPTION WHEN OTHERS THEN
			$12 = -255;
	END;
	
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 
