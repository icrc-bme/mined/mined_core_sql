CREATE OR REPLACE FUNCTION demographic.save_subject(
	IN id_mined_subject integer,
    IN id_institution integer,
    IN id_subject integer,
    IN subject_type character varying,
    IN sex character varying,
    IN handedness character varying,
    IN seizure_onset_age real,
    IN note character varying,
	IN subject_category character varying,
    OUT id_mined integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalog indices
	idx_type			 INTEGER;
	idx_sex				 INTEGER;
	idx_handedness  	 INTEGER;
	idx_subject_category INTEGER DEFAULT 1;
	
BEGIN

	--- Get subject type id
	IF subject_type IS NOT NULL THEN
		SELECT INTO idx_type demographic.subject_type.id FROM demographic.subject_type WHERE demographic.subject_type.type_name ILIKE $4;
		IF idx_type IS NULL THEN
			$10 = -201;
			RETURN;
		END IF;
	END IF;

	--- Get sex id
	IF sex IS NOT NULL THEN
		SELECT INTO idx_sex demographic.sex.id FROM demographic.sex WHERE demographic.sex.sex_name ILIKE $5;
		IF idx_sex IS NULL THEN
			$10 = -202;
			RETURN;
		END IF;
	END IF;

	--- Get handedness id
	IF handedness IS NOT NULL THEN
		SELECT INTO idx_handedness demographic.handedness.id FROM demographic.handedness WHERE demographic.handedness.handedness_name ILIKE $6;
		IF idx_handedness IS NULL THEN
			$10 = -203;
			RETURN;
		END IF;
	END IF;

	-- Get subject category id
	IF subject_category IS NULL THEN
		SELECT INTO idx_subject_category demographic.subject_category.id FROM demographic.subject_category WHERE demographic.subject_category.category_name ILIKE $11;
		IF idx_subject_category IS NULL THEN
			idx_subject_category = 1;			
		END IF;
	END IF;

	BEGIN
	IF $1 = 0 THEN
		INSERT INTO demographic.subject(
			id_institution,
			id_subject,
			id_subject_type,
			id_sex,
			id_handedness,
			seizure_onset_age,
			note,
			id_subject_category)
		VALUES (
			$2,
			$3,
			idx_type,
			idx_sex,
			idx_handedness,
			$7,
			$8,
			idx_subject_category)
		RETURNING
			demographic.subject.id_mined INTO $10;
	ELSE
		UPDATE demographic.subject SET 
			id_institution = $2,
			id_subject = $3,
			id_subject_type = idx_type,
			id_sex = idx_sex,
			id_handedness = idx_handedness,
			seizure_onset_age = $7,
			note = $8,
			id_race = idx_race,
			id_ethnicity = idx_ethnicity,
			id_subject_category = idx_subject_category
		WHERE  demographic.subject.id_mined = $1;
		$10 = $1;
	END IF;
	EXCEPTION WHEN OTHERS THEN
		$10 = -255;
	END;
	
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 
