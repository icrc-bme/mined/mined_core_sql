CREATE OR REPLACE FUNCTION demographic.save_subject_histopathologies(
	id_subject_histopathology integer,
	id_mined integer,
	dt_date_utc timestamp,
	histopathology_type character varying,
	histopathology_subtype character varying,
	OUT out_id_histopathology integer)
 RETURNS integer AS
$BODY$

DECLARE
	idx_histopathology		INTEGER DEFAULT 1;

BEGIN

	--- Get histopathology type id
	IF histopathology_type IS NOT NULL THEN
		SELECT INTO idx_histopathology demographic.histopathology.id FROM demographic.histopathology WHERE demographic.histopathology.histopathology_type ILIKE $4 AND demographic.histopathology.histopathology_subtype ILIKE $5;
		IF idx_histopathology IS NULL THEN
			$6 = -301;
			RETURN;
		END IF;
	END IF;

	BEGIN
		IF $1 = 0 THEN
			INSERT INTO demographic.subject_histopathologies( id_mined_subject, dt_date_utc, id_histopathology) VALUES( $2, $3, idx_histopathology) RETURNING demographic.subject_histopathologies.id INTO $6;
		ELSE
			UPDATE demographic.subject_histopathologies SET dt_date_utc = $3, id_histopathology = idx_histopathology WHERE demographic.subject_histopathologies.id = $1;
			$6 = $1;
		END IF;
	EXCEPTION WHEN OTHERS THEN
		$6 = -255;
	END;

RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
