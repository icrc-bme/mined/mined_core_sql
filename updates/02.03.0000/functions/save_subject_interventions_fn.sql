CREATE OR REPLACE FUNCTION demographic.save_subject_interventions(
    IN id_subject_intervention INTEGER,
    IN in_dt_date_utc TIMESTAMP,
	IN in_id_mined_subject INTEGER,
    IN intervention_name CHARACTER VARYING,
    IN ida_contact BIGINT[],
    OUT out_id_subject_interventions integer)
  RETURNS integer AS
$BODY$


DECLARE 

	--- Catalogs
	idx_intervention_name 	INTEGER default 1;

	--- Temporary values
	idx_intervention 		INTEGER default 0;
	idx_contact				INTEGER;
		
BEGIN	
	

	--- Get intervention id
	SELECT INTO idx_intervention_name demographic.intervention.id FROM demographic.intervention WHERE demographic.intervention.intervention_name ILIKE $4;
	IF idx_intervention_name IS NULL THEN
		$6 = -501;
		RETURN;
	END IF;


	BEGIN
		IF id_subject_intervention = 0 THEN

			INSERT INTO demographic.subject_interventions (
				dt_date_utc,
				id_intervention,
				id_mined_subject) 
			VALUES ($2,idx_intervention_name,$3) RETURNING demographic.subject_interventions.id INTO idx_intervention;

			IF ida_contact IS NOT NULL THEN
				FOREACH idx_contact IN ARRAY ida_contact
				LOOP
					INSERT INTO "space".contact_subjects_interventions (id_contact, id_subject_intervention ) VALUES (idx_contact,idx_intervention); 
				END LOOP;
			END IF;
			$6 = idx_intervention;

		ELSE
			DELETE FROM "space".contact_subjects_interventions WHERE "space".contact_subjects_interventions.id_subject_intervention = $1;

			UPDATE demographic.subject_interventions SET 
				dt_date_utc=$2,
				id_intervention=idx_intervention_name,
				id_mined_subject=$3
			WHERE demographic.subject_interventions.id=$1;

			IF ida_contact IS NOT NULL THEN
				FOREACH idx_contact IN ARRAY ida_contact
				LOOP
					INSERT INTO "space".contact_subjects_interventions (id_contact, id_subject_intervention ) VALUES (idx_contact, $1); 
				END LOOP;
			END IF;
			$6 = $1;
			
		END IF;
	EXCEPTION WHEN OTHERS THEN
		$6 = -255;
	END;
	
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
