CREATE OR REPLACE FUNCTION recording.save_trial(
	IN id_trial integer,
	IN onset integer,
    IN id_session integer,
    IN duration integer,
    IN trial_name character varying,
    IN task_name character varying,
    IN response_time integer,
    IN response character varying,
    IN ttl_value integer,
	OUT out_id_trial integer)
 RETURNS integer AS
$BODY$

DECLARE 
	--- Catalog indices
	idx_task		INTEGER default 1;
	idx_type		INTEGER default 1;

BEGIN	

	--- Get task id
	IF task_name IS NOT NULL THEN
		SELECT INTO idx_task recording.task.id FROM recording.task WHERE recording.task.task_name ILIKE $6;
		IF idx_task IS NULL THEN
			$10 = -701;
			RETURN;
		END IF;
	END IF;

	--- Get trial type id
	IF trial_name IS NOT NULL THEN
		SELECT INTO idx_type recording.trial_type.id FROM recording.trial_type WHERE recording.trial_type.type_name ILIKE $5 AND recording.trial_type.id_task = idx_task;
		IF idx_type IS NULL THEN
			$10 = -701;
			RETURN;
		END IF;
	END IF;

	BEGIN
		IF $1 = 0 THEN
			INSERT INTO recording.trial(
				onset,
				duration,
				response_time,
				response,
				ttl_value,
				id_trial_type,
				id_session)
			VALUES (
				$2,
				$4,
				$7,
				$8,
				$9,
				idx_type,
				$3)
			RETURNING
				recording.trial.id INTO $10;
		ELSE
			UPDATE recording.trial SET
				onset = $2,
				duration = $4,
				response_time = $7,
				response = $8,
				ttl_value = $9,
				id_trial_type = idx_type,
				id_session = $3
			WHERE  recording.trial.id = $1;
			$10 = $1;
		END IF;
	EXCEPTION WHEN OTHERS THEN
		$10 = -255;
	END;

RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

