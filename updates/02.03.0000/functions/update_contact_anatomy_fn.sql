DROP FUNCTION IF EXISTS space.update_contact_anatomy;
CREATE OR REPLACE FUNCTION space.update_contact_anatomy(id_contact bigint, anatomy_structure character varying, lobe character varying, hemisphere boolean, brain_area integer, manual boolean, atlas_type character varying, OUT out_id_anatomy integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
DECLARE 
	idx_anatomy 			int4 DEFAULT 0;
	idx_structure 			int4 DEFAULT 0;
	idx_lobe 				int4 DEFAULT 0;
	idx_atlas_type 			int4 DEFAULT 0;

BEGIN	
	
	--- check if a id_anatomy exists
	SELECT INTO idx_atlas_type space.atlas_type.id FROM space.atlas_type WHERE space.atlas_type.type_name ILIKE $7;
	IF idx_atlas_type = 0 OR idx_atlas_type IS NULL THEN
		$8 = -4;
		RETURN;
	END IF;
	
	SELECT INTO idx_structure space.anatomy_structure.id FROM space.anatomy_structure WHERE space.anatomy_structure.structure_name ILIKE $2;
	IF idx_structure = 0 OR idx_structure IS NULL THEN
		$8 = -5;
		RETURN;
	END IF;

	SELECT INTO idx_lobe space.lobe.id FROM space.lobe WHERE space.lobe.lobe_name ILIKE $3;
	IF idx_lobe = 0 OR idx_lobe IS NULL THEN
		$8 = -6;
		RETURN;
	END IF;

	--get id_anatomy ix exists

	SELECT INTO idx_anatomy a.id FROM space.anatomy AS a WHERE a.brain_area = $5 AND a.hemisphere = $4 AND a.id_anatomy_structure = idx_structure
		AND a.id_atlas_type = idx_atlas_type AND a.id_lobe = idx_lobe;
	IF idx_anatomy = 0 OR idx_anatomy IS NULL THEN
		-- add new id_anatomy -- extending catalog
		INSERT INTO space.anatomy(hemisphere, brain_area, id_atlas_type, id_anatomy_structure, id_lobe) VALUES( $4, $5, idx_atlas_type, idx_structure, idx_lobe) RETURNING space.anatomy.id INTO idx_anatomy;
	END IF;


	BEGIN
		UPDATE space.anatomy_contact SET id_anatomy = idx_anatomy, manual_assign = $6
							WHERE  space.anatomy_contact.id_contact = $1;
	$8 = idx_anatomy;
	
--	EXCEPTION 
--		WHEN unique_violation THEN 
--			$8 = -1;

--		WHEN OTHERS THEN
--			$8 = -253;
	END;

RETURN;
END
$function$
;