DROP FUNCTION IF EXISTS recording.update_session;
CREATE OR REPLACE FUNCTION recording.update_session(id_session integer, session_name character varying, session_type character varying, device character varying, modality character varying, reference character varying, note character varying, ip_address character varying, OUT out_id_session integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$

DECLARE 
	idx_type			INTEGER DEFAULT 0;
	idx_modality		INTEGER DEFAULT 0;
	idx_reference		INTEGER DEFAULT 0;

BEGIN	

	SELECT INTO idx_type recording.session_type.id FROM recording.session_type WHERE recording.session_type.type_name ILIKE $3;
	IF idx_type = 0 OR idx_type IS NULL THEN
		$9 = -1;
		RETURN;
	END IF;

	SELECT INTO idx_modality recording.modality.id FROM recording.modality WHERE recording.modality.modality_name ILIKE $5;
	IF idx_type = 0 OR idx_type IS NULL THEN
		$9 = -2;
		RETURN;
	END IF;

	SELECT INTO idx_reference signal.recording_reference.id FROM signal.recording_reference WHERE signal.recording_reference.reference_name ILIKE $6;
	IF idx_type = 0 OR idx_type IS NULL THEN
		$9 = -3;
		RETURN;
	END IF;

	BEGIN
		UPDATE recording."session" SET session_name = $2,
								id_session_type = idx_type,
								acquisition_device = $4,
								id_modality = idx_modality,
								id_recording_reference = idx_reference,
								ip_address = $8,
								note = $7 WHERE  recording."session".id = $1;
		$9 = $1;
	EXCEPTION WHEN OTHERS THEN
		$9 = -255;
	END;

RETURN;
END
$function$
;