CREATE OR REPLACE FUNCTION signal.upsert_annotation_channel(
    IN dt_start TIMESTAMP,
	IN dt_stop TIMESTAMP,								
	IN annotation_text character varying,
	IN ida_channel bigint[],
    IN annotation_type character varying,
	IN annotation_subtype character varying,
    OUT id_annotation integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Temporary values
	idx_channel			INTEGER;
    idx_annotation_channel			INTEGER DEFAULT 1;

BEGIN

    --- Get annotation channel type id
	SELECT INTO idx_annotation_channel signal.annotation_channel_type.id FROM signal.annotation_channel_type WHERE signal.annotation_channel_type.type_name ILIKE $5 AND signal.annotation_channel_type.subtype_name ILIKE $6;
	IF idx_annotation_channel = 0 OR idx_annotation_channel IS NULL THEN
		$7 = -1501;
		RETURN;
	END IF;

	FOREACH idx_channel IN ARRAY ida_channel
		LOOP
			INSERT INTO signal.annotation_channel(
				event_start_utc,
				event_stop_utc,
				annotation_text,
			    id_annotation_channel_type,
				id_channel)
			VALUES (
				$1,
				$2,
				$3,
                idx_annotation_channel,
				idx_channel)
			ON CONFLICT ON CONSTRAINT annotation_channel_un DO UPDATE SET
				annotation_text = $3;
		END LOOP;

	$7 = 1;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 
