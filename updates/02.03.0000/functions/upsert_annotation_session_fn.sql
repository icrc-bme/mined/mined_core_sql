CREATE OR REPLACE FUNCTION recording.upsert_annotation_session(
    IN event_start_utc TIMESTAMP,
    IN id_session integer,
    IN event_stop_utc TIMESTAMP,
    IN annotation_text character varying,
    IN annotation_type character varying,
	IN annotation_subtype character varying,
    OUT id_annotation integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalog indices
    idx_annotation_session			INTEGER DEFAULT 1;

BEGIN

    --- Get annotation session type id
	SELECT INTO idx_annotation_session recording.annotation_session_type.id FROM recording.annotation_session_type WHERE recording.annotation_session_type.type_name ILIKE $5 AND recording.annotation_session_type.subtype_name ILIKE $6;
	IF idx_annotation_session = 0 OR idx_annotation_session IS NULL THEN
		$7 = -1401;
		RETURN;
	END IF;

	INSERT INTO recording.annotation_session(
		event_start_utc,
		event_stop_utc,
		annotation_text,
	    id_annotation_session_type,
	    id_session)
	VALUES (
		$1,
		$3,
		$4,
		idx_annotation_session,
        $2)
	ON CONFLICT ON CONSTRAINT annotation_session_un DO UPDATE SET
		annotation_text = $4
	RETURNING
		recording.annotation_session.id INTO $7;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 
