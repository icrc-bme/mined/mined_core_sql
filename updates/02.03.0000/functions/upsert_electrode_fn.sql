DROP FUNCTION IF EXISTS "space".upsert_electrode;
CREATE OR REPLACE FUNCTION "space".upsert_electrode(
		IN electrode_name character varying,
		IN id_mined integer,
		IN type_name character varying,
		IN manufacturer character varying,
		IN dimension_a float,
    IN dimension_b float,
    IN contact_material character varying,
    IN macro_contact_distance float,
    IN micro_contact_distance float,
    OUT id integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalogs
	idx_electrode_type			INTEGER default 1;

BEGIN	

	--- Get electrode_type id
	SELECT INTO
		idx_electrode_type "space".electrode_type.id
	FROM
		"space".electrode_type
	WHERE
		"space".electrode_type.type_name ILIKE $3
		AND "space".electrode_type.manufacturer ILIKE $4
		AND "space".electrode_type.dimension_a = $5
		AND "space".electrode_type.dimension_b = $6;

	IF idx_electrode_type IS NULL THEN
		INSERT INTO
			"space".electrode_type(type_name, manufacturer, dimension_a, dimension_b, contact_material, macro_contact_distance, micro_contact_distance)
		VALUES
			($3, $4, $5, $6, $7, $8, $9)
		ON CONFLICT ON CONSTRAINT electrode_type_un DO UPDATE SET
			contact_material = $7,
			macro_contact_distance = $8,
			micro_contact_distance = $9
		RETURNING "space".electrode_type.id INTO idx_electrode_type;
	END IF;

	
	INSERT INTO "space".electrode(
		electrode_name,
		id_electrode_type,
		id_mined_subject) 
	VALUES (
		$1,
		idx_electrode_type,
		$2)
	ON CONFLICT ON CONSTRAINT electrode_un DO UPDATE SET-- DO NOTHING does not work with RETURNING
		electrode_name = $1
	RETURNING
		"space".electrode.id INTO $10;
	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;