CREATE OR REPLACE FUNCTION recording.upsert_session(
		IN id_mined_subject integer,
		IN dt_start_utc_in timestamp,
		IN dt_stop_utc_in timestamp,
    IN acquisition_device character varying,
    IN ip_address character varying,
    IN session_name character varying,
    IN note character varying,
    IN session_type character varying,
    IN modality character varying,
    IN recording_reference character varying,
    OUT id integer)
  RETURNS integer AS
$BODY$

DECLARE 

	--- Catalogs
	idx_session_type			INTEGER default 1;
	idx_modality				INTEGER default 1;
	idx_recording_reference		INTEGER default 1;

	--- Temporary values
	idx_session				INTEGER default 0;
	idx_electrode				INTEGER;

BEGIN	

	--- Get session type id
	IF session_type IS NOT NULL THEN
		SELECT INTO idx_session_type recording.session_type.id FROM recording.session_type WHERE recording.session_type.type_name ILIKE $8;
		IF idx_session_type IS NULL THEN
			INSERT INTO recording.session_type(type_name) VALUES ($8) RETURNING recording.session_type.id INTO idx_session_type;
		END IF;
	END IF;

	--- Get modality id
	IF modality IS NOT NULL THEN
		SELECT INTO idx_modality recording.modality.id FROM recording.modality WHERE recording.modality.modality_name ILIKE $9;
		IF idx_modality IS NULL THEN
			INSERT INTO recording.modality(modality_name) VALUES ($9) RETURNING recording.modality.id INTO idx_modality;
		END IF;
	END IF;

	-- Get recording reference id
	IF recording_reference IS NOT NULL THEN
		SELECT INTO idx_recording_reference signal.recording_reference.id FROM signal.recording_reference WHERE signal.recording_reference.reference_name ILIKE $10;
		IF idx_recording_reference IS NULL THEN
			INSERT INTO signal.recording_reference(reference_name) VALUES ($10) RETURNING signal.recording_reference.id INTO idx_recording_reference;
		END IF;
	END IF;

	INSERT INTO recording.session(
		dt_start_utc,
		dt_stop_utc,
		acquisition_device,
		ip_address,
		session_name,
		note,
		id_session_type,
		id_modality,
		id_recording_reference,
		id_mined_subject) 
	VALUES (
		dt_start_utc_in,
		dt_stop_utc_in,
		$4,
		$5,
		$6,
		$7,
		idx_session_type,
		idx_modality,
		idx_recording_reference,
		$1)
	ON CONFLICT ON CONSTRAINT session_un DO UPDATE SET
		dt_stop_utc = dt_stop_utc_in,
		acquisition_device = $4,
		ip_address = $5,
		session_name = $6,
		note = $7,
		id_session_type = idx_session_type,
		id_modality = idx_modality,
		id_recording_reference = idx_recording_reference
	RETURNING
		recording.session.id INTO $11;

	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
