CREATE OR REPLACE FUNCTION demographic.upsert_subject_histopathologies(
	id_mined integer,
	in_dt_date_utc timestamp,
	histopathology_type character varying,
	histopathology_subtype character varying,
	OUT out_id_histopathology integer)
 RETURNS integer AS
$BODY$

DECLARE 
	idx_histopathology		INTEGER DEFAULT 1;

BEGIN	
	--- Get histopathology type id
	IF histopathology_type IS NOT NULL THEN
		SELECT INTO idx_histopathology demographic.histopathology.id FROM demographic.histopathology WHERE demographic.histopathology.histopathology_type ILIKE $3 AND demographic.histopathology.histopathology_subtype ILIKE $4;
		IF idx_histopathology IS NULL THEN
			INSERT INTO demographic.histopathology(histopathology_type, histopathology_subtype) VALUES ($3, $4) RETURNING id INTO idx_histopathology;
		END IF;
	END IF;

	INSERT INTO demographic.subject_histopathologies(
		dt_date_utc,
		id_histopathology,
		id_mined_subject) 
	VALUES (
		$2,
		idx_histopathology,
		$1)
	ON CONFLICT ON CONSTRAINT subject_histopathologies_un DO NOTHING
	RETURNING
		demographic.subject_histopathologies.id INTO $5;

	IF $5 IS NULL THEN
		SELECT demographic.subject_histopathologies.id INTO $5 FROM demographic.subject_histopathologies WHERE
			dt_date_utc = $2
			AND id_histopathology = idx_histopathology
			AND id_mined_subject = $1;
	END IF;
	RETURN;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

