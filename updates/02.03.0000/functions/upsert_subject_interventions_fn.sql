CREATE OR REPLACE FUNCTION demographic.upsert_subject_interventions(
    IN in_dt_date_utc TIMESTAMP,
	  IN in_id_mined_subject INTEGER,
    IN intervention_name CHARACTER VARYING,
    IN ida_contact BIGINT[],
    OUT out_id_subject_interventions integer)
  RETURNS integer AS
$BODY$


DECLARE 

	--- Catalogs
	idx_intervention_name 	INTEGER default 1;

	--- Temporary values
	idx_contact				INTEGER;
		
BEGIN	
	

	--- Get intervention id
	SELECT INTO idx_intervention_name demographic.intervention.id FROM demographic.intervention WHERE demographic.intervention.intervention_name ILIKE $3;
	IF idx_intervention_name IS NULL THEN
		INSERT INTO demographic.intervention(intervention_name) VALUES ($3) RETURNING demographic.intervention.id INTO idx_intervention_name;
	END IF;

	INSERT INTO demographic.subject_interventions(
		dt_date_utc,
		id_mined_subject,
		id_intervention)
	VALUES (
		$1,
		$2,
		idx_intervention_name)
	ON CONFLICT ON CONSTRAINT subject_interventions_un DO NOTHING
	RETURNING
		demographic.subject_interventions.id INTO $5;

	IF $5 IS NULL THEN
		SELECT demographic.subject_interventions.id INTO $5 FROM demographic.subject_interventions WHERE
			dt_date_utc = $1
			AND id_mined_subject = $2
			AND id_intervention = idx_intervention_name;
	END IF;

	DELETE FROM "space".contact_subjects_interventions WHERE "space".contact_subjects_interventions.id_subject_intervention = $5;

	IF ida_contact IS NOT NULL THEN
		FOREACH idx_contact IN ARRAY ida_contact
		LOOP
			INSERT INTO "space".contact_subjects_interventions (id_contact, id_subject_intervention ) VALUES (idx_contact, $5); 
		END LOOP;
	END IF;

	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
