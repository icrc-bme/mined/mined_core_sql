DROP VIEW  IF EXISTS signal.annotation_channel_view;         
CREATE OR REPLACE VIEW signal.annotation_channel_view
AS SELECT ac.id,
    c.id_session,
    ac.event_start_utc,
    ac.event_stop_utc,
    ac.annotation_text,
    act.type_name,
    act.subtype_name,
    ac.id_channel,
    c.channel_name,
    tz.zone_name
   FROM signal.annotation_channel ac
     JOIN signal.annotation_channel_type act ON ac.id_annotation_channel_type = act.id
     JOIN signal.channel c ON ac.id_channel = c.id
     JOIN recording.session s ON c.id_session = s.id
     JOIN demographic.subject s2 ON s.id_mined_subject = s2.id_mined
     JOIN demographic.institution i ON s2.id_institution = i.id
     JOIN demographic.time_zone tz ON i.id_time_zone = tz.id;  