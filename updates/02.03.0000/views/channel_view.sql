CREATE OR REPLACE VIEW signal.channel_view
AS SELECT c.id,
    c.id_session,
    c.channel_name,
    c.sampling_frequency,
    qt.type_name AS quality_type,
    ct.type_name AS channel_type,
    c.low_cutoff,
    c.high_cutoff,
    c.unit,
    c.acquisition_number
   FROM signal.channel c
     JOIN signal.channel_type ct ON c.id_channel_type = ct.id
     JOIN signal.quality_type qt ON c.id_quality_type = qt.id;
