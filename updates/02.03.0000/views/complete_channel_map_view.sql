DROP VIEW IF EXISTS signal.complete_channel_map_view;   

CREATE OR REPLACE VIEW signal.complete_channel_map_view
AS SELECT inst.id AS id_institution,
    inst.institution_name,
    (inst.patient_prefix || LPAD (cast (s.id_subject as varchar), 5, '0')) AS id_subject,
    s2.id AS id_session,
    st.type_name AS session_type,
    c.id AS id_channel,
    c.channel_name,
    c.sampling_frequency,
    c.acquisition_number,
    ct.type_name AS channel_type,
    qt.type_name AS signal_quality,
    rr.reference_name,
    c2.id AS id_contact,
    concat(e.electrode_name, c2.contact_number::character varying) AS contact_name,
    ct2.type_name AS contact_type,
    cs2.size_name AS contact_size,
    c3.c_x,
    c3.c_y,
    c3.c_z,
    cs.system_name,
    as2.structure_name AS anatomy_structure,
    a.hemisphere,
    l.lobe_name,
    at2.type_name AS atlas_type,
    ip.pathology_name AS imaging_pathology,
    i.intervention_name AS intervention,
    seiz.subtype_name AS seizure_info,
    is2.scale_name AS latest_ilae,
    es.scale_name AS latest_engel,
    ms.scale_name AS latest_mchugh
   FROM demographic.institution inst
     JOIN demographic.subject s ON inst.id = s.id_institution
     LEFT JOIN recording.session s2 ON s.id_mined = s2.id_mined_subject
     JOIN recording.session_type st ON s2.id_session_type = st.id
     LEFT JOIN signal.channel c ON s2.id = c.id_session
     JOIN signal.channel_type ct ON c.id_channel_type = ct.id
     JOIN signal.quality_type qt ON c.id_quality_type = qt.id
     LEFT JOIN signal.channel_contact cc ON c.id = cc.id_channel
     LEFT JOIN signal.recording_reference rr ON cc.id_recording_reference = rr.id
     LEFT JOIN space.contact c2 ON cc.id_contact = c2.id
     JOIN space.electrode e ON c2.id_electrode = e.id
     JOIN space.contact_type ct2 ON ct2.id = c2.id_contact_type
     JOIN space.contact_size cs2 ON cs2.id = c2.id_contact_size
     LEFT JOIN space.coordinates c3 ON c2.id = c3.id_contact
     LEFT JOIN space.coordinate_system cs ON c3.id_coordinate_system = cs.id
     LEFT JOIN space.anatomy_contact ac ON c2.id = ac.id_contact
     LEFT JOIN space.anatomy a ON ac.id_anatomy = a.id
     LEFT JOIN space.anatomy_structure as2 ON a.id_anatomy_structure = as2.id
     LEFT JOIN space.atlas_type at2 ON a.id_atlas_type = at2.id
     LEFT JOIN space.lobe l ON a.id_lobe = l.id
     LEFT JOIN space.imaging_pathology ip ON c2.id_imaging_pathology = ip.id
     LEFT JOIN space.contact_subjects_interventions csi ON csi.id_contact = c2.id
     LEFT JOIN demographic.subject_interventions si ON csi.id_subject_intervention = si.id
     LEFT JOIN demographic.intervention i ON si.id_intervention = i.id
     LEFT JOIN ( SELECT DISTINCT ac_1.id_channel,
            act.subtype_name
           FROM signal.annotation_channel ac_1
             JOIN signal.annotation_channel_type act ON ac_1.id_annotation_channel_type = act.id
          WHERE act.type_name::text = 'seizure'::text) seiz ON c.id = seiz.id_channel
     LEFT JOIN ( SELECT so.id_mined_subject,
            so.id_ilae_scale,
            so.id_engel_scale,
            so.id_mchugh_scale
           FROM demographic.subject_outcomes so
             JOIN ( SELECT subject_outcomes.id_mined_subject,
                    max(subject_outcomes.dt_date_utc) AS max_date
                   FROM demographic.subject_outcomes
                  GROUP BY subject_outcomes.id_mined_subject) somd ON so.id_mined_subject = somd.id_mined_subject AND so.dt_date_utc = somd.max_date) o ON o.id_mined_subject = s.id_mined
     LEFT JOIN demographic.ilae_scale is2 ON o.id_ilae_scale = is2.id
     LEFT JOIN demographic.engel_scale es ON o.id_engel_scale = es.id
     LEFT JOIN demographic.mchugh_scale ms ON o.id_mchugh_scale = ms.id;