CREATE OR REPLACE VIEW "space".contact_view
AS SELECT c.id,
    e.id_mined_subject,
    c.contact_number,
    c.impedance,
    ct.type_name,
    cs.size_name,
    cs.size_value,
    ip.pathology_name,
    c2.c_x,
    c2.c_y,
    c2.c_z,
    cs2.system_name AS coordinate_name,
    cs2.system_unit AS coordinate_unit,
    a.hemisphere,
    a.brain_area,
    ac.manual_assign,
    a1.type_name AS atlas_name,
    a2.structure_name,
    l.lobe_name,
    e.id AS id_electrode,
    e.electrode_name
   FROM space.contact c
     JOIN space.contact_size cs ON c.id_contact_size = cs.id
     JOIN space.contact_type ct ON c.id_contact_type = ct.id
     JOIN space.coordinates c2 ON c.id = c2.id_contact
     JOIN space.coordinate_system cs2 ON c2.id_coordinate_system = cs2.id
     JOIN space.anatomy_contact ac ON c.id = ac.id_contact
     JOIN space.anatomy a ON ac.id_anatomy = a.id
     JOIN space.atlas_type a1 ON a.id_atlas_type = a1.id
     JOIN space.electrode e ON c.id_electrode = e.id
     JOIN space.anatomy_structure a2 ON a.id_anatomy_structure = a2.id
     JOIN space.lobe l ON a.id_lobe = l.id
     JOIN space.imaging_pathology ip ON ip.id = c.id_imaging_pathology;