DROP VIEW IF EXISTS demographic.institution_view;
CREATE OR REPLACE VIEW demographic.institution_view
AS SELECT i.id,
    i.institution_name,
    i.patient_prefix,
    tz.zone_name
   FROM demographic.institution i
     JOIN demographic.time_zone tz ON i.id_time_zone = tz.id;
