DROP VIEW  IF EXISTS demographic.irb_view;     
CREATE OR REPLACE VIEW demographic.irb_view 
AS SELECT irb.irb_num, 
    irb.irb_date, 
    (i.patient_prefix || LPAD (cast (s.id_subject as varchar), 5, '0')) AS id_subject
FROM demographic.irb_subject irb
JOIN demographic.subject s ON irb.id_mined_subject  = s.id_mined
JOIN demographic.institution i ON s.id_institution = i.id;
