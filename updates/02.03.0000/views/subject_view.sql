DROP VIEW  IF EXISTS demographic.subject_view;     
CREATE OR REPLACE VIEW demographic.subject_view
AS SELECT sx.sex_name,
    h.handedness_name,
    s.note,
    (i2.patient_prefix || LPAD (cast (s.id_subject as varchar), 5, '0')) AS id_subject,
    s.id_mined,
    s.seizure_onset_age,
    i2.institution_name,
    st.type_name,
    ( SELECT count(*) AS count
           FROM demographic.subject_interventions
          WHERE subject_interventions.id_mined_subject = s.id_mined) AS intervention_count,
    ( SELECT count(*) AS count
           FROM demographic.subject_histopathologies
          WHERE subject_histopathologies.id_mined_subject = s.id_mined) AS histopathology_count,
    ( SELECT count(*) AS count
           FROM demographic.subject_outcomes
          WHERE subject_outcomes.id_mined_subject = s.id_mined) AS outcome_count,
    i2.id AS id_institution,
    sc.category_name
   FROM demographic.subject s
     JOIN demographic.institution i2 ON s.id_institution = i2.id
     JOIN demographic.subject_type st ON s.id_subject_type = st.id
     JOIN demographic.sex sx ON s.id_sex = sx.id
     JOIN demographic.handedness h ON s.id_handedness = h.id
     JOIN demographic.subject_category sc ON s.id_subject_category = sc.id;